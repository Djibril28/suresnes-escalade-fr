# Settings
WORDPRESS=/homez.577/suresnes/wordpress
BACKUP=/homez.577/suresnes/wordpress-backups

# Backup files
BACKUP_NAME=$(command date +"%Y-%m-%d")
#mkdir ${BACKUP}

# Backup database
DB_HOST=$(command grep "DB_HOST" ${WORDPRESS}/wp-config.php | command sed -e "s/^.*,[ ]*'\(.*\)'.*$/\1/")
DB_USER=$(command grep "DB_USER" ${WORDPRESS}/wp-config.php | command sed -e "s/^.*,[ ]*'\(.*\)'.*$/\1/")
DB_PASSWORD=$(command grep "DB_PASSWORD" ${WORDPRESS}/wp-config.php | command sed -e "s/^.*,[ ]*'\(.*\)'.*$/\1/")
DB_NAME=$(command grep "DB_NAME" ${WORDPRESS}/wp-config.php | command sed -e "s/^.*,[ ]*'\(.*\)'.*$/\1/")
command mysqldump -u ${DB_USER} --password=${DB_PASSWORD} -h ${DB_HOST} \
    --databases ${DB_NAME} > ${BACKUP}/database.sql
command tar -cjvf ${BACKUP}/${BACKUP_NAME}-database.tar.bz2 ${BACKUP}/database.sql
