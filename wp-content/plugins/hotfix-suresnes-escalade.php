<?php
/*
Plugin Name: HotFix Suresnes Escalade
Plugin URI: http://suresnes-escalade.fr/
Description: Plugin pour corriger les bugs des autres plugins sans les modifier.
Author: Jérôme TAMARELLE
Version: 0
Author URI: http://jerome.tamarelle.net/
*/

function hotfix_custom_colors() {
   echo <<<CSS
<style type="text/css">
.ui-widget-overlay.ui-front {
    display: none;
}
</style>
CSS;
}

add_action('admin_head', 'hotfix_custom_colors');

