<?php

global $page_id,$widget_layout,$feature_block_layout;

// Page Layout
$page_layout = get_post_meta($page_id, '_page_layout', true);
$page_layout = $page_layout ? $page_layout[0] : 'testimonials_featured_widgets';
$page_layout_order = explode('_',$page_layout);
$page_content = get_the_content($page_id);

$other_options = get_post_meta($page_id, '_page_options', true);

// Page Widget Settings
$widget_layout = get_post_meta($page_id, '_widget_layout', true);
$widget_layout = $widget_layout ? $widget_layout[0] : 'no-widgets';

// Page Widget Settings
$feature_block_layout = get_post_meta($page_id, '_feature_block_layout', true);
$feature_block_layout = $feature_block_layout ? $feature_block_layout[0] : 'no-features';

// Loop through page layout pieces
foreach ($page_layout_order as $layout_type){
	
	// Add double lines where they look good
	if (isset($previous_layout_type) && $layout_type == 'page' && $previous_layout_type == 'widgets' || isset($previous_layout_type) && $layout_type == 'widgets' && $previous_layout_type == 'page' && $page_content ){
		$include_hr = true;
	} else {
		$include_hr = false;
	}
	
	// Load the page piece
	call_user_func_array('page_part_'.$layout_type,array($page_id,$include_hr));
	$previous_layout_type = $layout_type;
	
}