<?php
	
global $page_id;

// Slider Choice
$slider_choice = get_post_meta($page_id, '_slider_choice', true);
$featured_thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'page-banner' );
$featured_thumbnail_src = $featured_thumbnail_src[0];

if ($featured_thumbnail_src && !$slider_choice) :
	echo '<section class="top-image" style="background:url('.$featured_thumbnail_src.') no-repeat top center; background-size:cover;"></section>';
endif;

if ($slider_choice):
	gymboom_slider($slider_choice);
endif;