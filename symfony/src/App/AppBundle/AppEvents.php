<?php

namespace App\AppBundle;

final class AppEvents
{
    const SUBSCRIPTION_UPDATED = 'app.subscription.updated';

    const SUBSCRIPTION_SUBMITTED = 'app.subscription.submitted';

    const SUBSCRIPTION_VALIDATED = 'app.subscription.validated';

    const SUBSCRIPTION_CANCELLED = 'app.subscription.cancelled';

    private function __construct() {}
}