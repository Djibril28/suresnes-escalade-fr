<?php

namespace App\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubscriptionInsuranceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('licence', 'integer', array(
                'label' => 'Numéro de licence',
                'required' => false,
            ))
            ->add('insurance', 'choice', array(
                'label' => 'Assurance',
                'expanded' => true,
                'choices' => array(
                    0 => 'Sans assurance (nécessite un justificatif d\'assurance)',
                    1 => 'Assurance base',
                    2 => 'Assurance base +',
                    3 => 'Assurance base ++',
                )
            ))
            ->add('insurance_ij', 'choice', array(
                'label' => 'Option IJ',
                'expanded' => true,
                'choices' => array(
                    1 => 'Option IJ 1',
                    2 => 'Option IJ 2',
                    3 => 'Option IJ 3',
                )
            ))
            ->add('insurance_slackline', 'checkbox', array(
                'label' => 'Option assurance SLACKLINE',
                'required' => false,
            ))
            ->add('insurance_ski', 'checkbox', array(
                'label' => 'Option assurance SKI',
                'required' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AppBundle\Entity\Subscription'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_appbundle_subscription';
    }
}
