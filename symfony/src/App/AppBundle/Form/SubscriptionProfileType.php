<?php

namespace App\AppBundle\Form;

use App\AppBundle\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubscriptionProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('address')
            ->add('postalCode')
            ->add('city')
            ->add('phone')
            ->add('sex', 'choice', array('choices' => array('M' => 'Masculin', 'F' => 'Féminin')))
            ->add('birthday', 'birthday')
            ->add('address', 'textarea')
            ->add('postalCode', 'integer')
            ->add('city')
            ->add('phone')
            ->add('email', 'email')
            ->add('occupation')
            ->add('climbingLevel', 'choice', array('choices' => Subscription::getClimbingLevels()))
            ->add('climbingExperience', 'choice', array('choices' => Subscription::getClimbingExperiences()))
            ->add('passportFfme', 'choice', array('choices' => Subscription::getPassportsFFME()))
            ->add('isCompetitor', 'choice', array('choices' => array(0 => 'Non', 1 => 'Oui'), 'expanded' => true))
            ->add('licence', 'integer', array('required' => false))
            // ->add('freeComment')
            ->add('parentFirstName')
            ->add('parentLastName')
            ->add('parentPhone')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AppBundle\Entity\Subscription',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_appbundle_subscription';
    }
}
