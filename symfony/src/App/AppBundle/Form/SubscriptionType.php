<?php

namespace App\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubscriptionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName')
            ->add('firstName')
            ->add('sex')
            ->add('isActive')
            ->add('isRenewal')
            ->add('createdAt')
            ->add('activatedAt')
            ->add('activatedBy')
            ->add('birthday')
            ->add('age')
            ->add('occupation')
            ->add('licence')
            ->add('certificateFile')
            ->add('certificateDoctor')
            ->add('certificateDate')
            ->add('user')
            ->add('season')
            ->add('slot1')
            ->add('slot2')
            ->add('activatedSlot')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AppBundle\Entity\Subscription'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_appbundle_subscription';
    }
}
