<?php

namespace App\AppBundle\Form;

use App\AppBundle\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SimpleSubscriptionType extends AbstractType
{

    private $season;

    public function __construct($season)
    {
        $this->season = $season;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName', 'text')
            ->add('firstName', 'text')
            ->add('birthday', 'birthday')
            ->add('sex', 'choice', array('choices' => array('M' => 'Masculin', 'F' => 'Féminin')))
            ->add('nationality', 'choice', array('choices' => Subscription::getNationalityChoices()))
            ->add('address', 'text')
            // ->add('address2', 'text')
            ->add('postalCode', 'text')
            ->add('city', 'text')
            // ->add('country', 'choice', array('choices' => array('France' => 'France')))
            ->add('phone', 'text')
            ->add('email', 'email')
            ->add('occupation')
            ->add('isRenewal', 'choice', array('choices' => array(0 => 'Non', 1 => 'Oui')))
        ;

        $builder
            ->add('parentLastName', 'text', array('required' => false))
            ->add('parentFirstName', 'text', array('required' => false))
            ->add('parentPhone', 'text', array('required' => false))
            ->add('parentEmail', 'email', array('required' => false))
        ;

        $options = array(
            'choices' => $this->season->getSlots()->filter(function ($slot) {
                return (!$slot->getType() || !$slot->getType()->getIsRestricted()) && !$slot->getIsFull();
            }),
            'property' => 'label',
            'required' => true,
            'empty_value' => '-',
            // 'expanded' => true,
        );

        $builder
            ->add('slot1', null, $options)
            ->add('slot2', null, $options)
            ->add('noSlot', 'checkbox', array('required' => false))
        ;

        $builder
            ->add('climbingLevel', 'choice', array('choices' => Subscription::getClimbingLevels()))
            ->add('climbingExperience', 'choice', array('choices' => Subscription::getClimbingExperiences()))
            ->add('passportFfme', 'choice', array('choices' => Subscription::getPassportsFFME()))
            ->add('isCompetitor', 'choice', array('choices' => Subscription::getIsCompetitorChoices()))
        ;

        $builder
            ->add('licence', 'text', array('required' => false))
            ->add('insurance', 'choice', array('choices' => $this->season->getInsuranceChoicesForSubscription()))
            ->add('insuranceIj', 'choice', array('choices' => $this->season->getInsuranceIjChoices()))
            ->add('insuranceSlackline', 'choice', array('choices' => $this->season->getInsuranceSlacklineChoices()))
            ->add('insuranceSki', 'choice', array('choices' => $this->season->getInsuranceSkiChoices()))
        ;

        $builder
            ->add('payment3Times', 'checkbox', array('required' => false))
            ->add('paymentPass92', 'checkbox', array('required' => false))
            ->add('paymentCouponSport', 'checkbox', array('required' => false))
            ->add('paymentSubvensionCe', 'checkbox', array('required' => false))
        ;

        $builder
            // ->add('certificateFile', 'file', array('data_class' => null, 'required' => false))
            ->add('certificateDoctor', 'text')
            ->add('certificateDate', 'date')
            ->add('certificateCompetition', 'checkbox', array('required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AppBundle\Entity\Subscription'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_appbundle_subscription';
    }
}
