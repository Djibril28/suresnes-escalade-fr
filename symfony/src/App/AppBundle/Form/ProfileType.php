<?php

namespace App\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\AppBundle\Entity\Profile;

class ProfileType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('sex', 'choice', array('choices' => array('M' => 'Masculin', 'F' => 'Féminin')))
            ->add('birthday', 'birthday')
            ->add('address', 'textarea')
            ->add('postalCode', 'integer')
            ->add('city')
            ->add('phone')
            ->add('email', 'email')
            ->add('occupation')
            ->add('climbingLevel', 'choice', array('choices' => Profile::getClimbingLevels()))
            ->add('climbingExperience', 'choice', array('choices' => Profile::getClimbingExperiences()))
            ->add('passportFfme', 'choice', array('choices' => Profile::getPassportsFFME()))
            ->add('isCompetitor', 'choice', array('choices' => array(0 => 'Non', 1 => 'Oui'), 'expanded' => true))
            ->add('licence', 'integer', array('required' => false))
            //->add('freeComment')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AppBundle\Entity\Profile'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_appbundle_profile';
    }
}
