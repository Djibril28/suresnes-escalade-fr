<?php

namespace App\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubscriptionSlotType extends AbstractType
{
    private $subscription;

    public function __construct($subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $season = $this->subscription->getSeason();

        $options = array(
            'choices' => $season->getSlots()->filter(function ($slot) {
                return !$slot->getType() || !$slot->getType()->getIsRestricted();
            }),
            'property' => 'title',
            'required' => true,
            'empty_value' => '-',
            // 'expanded' => true,
        );

        $builder
            ->add('slot1', null, $options)
            ->add('slot2', null, $options)
            ->add('no_slot', 'checkbox', array('label' => 'Pas de créneaux, juste une licence.', 'required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AppBundle\Entity\Subscription',
            'validation_groups' => array('slots'),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_appbundle_subscription_slots';
    }
}
