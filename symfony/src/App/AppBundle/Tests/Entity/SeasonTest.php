<?php

namespace App\AppBundle\Tests\Entity;

use App\AppBundle\Entity\Season;
use App\AppBundle\Entity\Subscription;

class SeasonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider dataForSubscriptionPrice
     */
    public function testSubscriptionPrice(array $data, $expected)
    {
        $season = new Season();
        $season->setBeginAt(new \DateTime('2014-06-01'));
        $season->setEndAt(new \DateTime('2015-09-01'));
        $season->setPriceLicenceChild(30.45);
        $season->setPriceLicenceAdult(43.15);
        $season->setPriceLicenceFamily(11.5);
        $season->setPriceFirstRegistration(30);
        $season->setPriceNotResidentChild(20);
        $season->setPriceNotResidentAdult(20);
        $season->setPriceCommittee(6.25);
        $season->setPriceClubChild(178.3);
        $season->setPriceClubAdult(180.6);
        $season->setPriceInsurance0(3);
        $season->setPriceInsurance1(10);
        $season->setPriceInsurance2(13);
        $season->setPriceInsurance3(20);
        $season->setPriceInsuranceSki(5);
        $season->setPriceOptionIJ1(18);
        $season->setPriceOptionIJ2(30);
        $season->setPriceOptionIJ3(35);

        $subscription = new Subscription();
        $subscription->setSeason($season);
        $subscription->setIsRenewal($data['isRenewal']);
        $subscription->setBirthday(new \DateTime($data['birthday']));
        $subscription->setInsurance($data['insurance']);
        $subscription->setInsuranceIj($data['insuranceIj']);
        $subscription->setInsuranceSki($data['insuranceSki']);
        $subscription->setCity($data['city']);
        $subscription->setLicenceFamily($data['family']);

        $price = $season->getSubscriptionPrice($subscription);

        $this->assertEquals($expected, $price);
    }

    public function dataForSubscriptionPrice()
    {
        return array(
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                240,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '2000-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                225,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '1997-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                240,
            ),

            // Non Suresnois
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'puteaux', 'family' => false),
                260,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '2000-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'puteaux', 'family' => false),
                245,
            ),

            // Nouvelle inscription
            array(
                array('isRenewal' => false, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                270,
            ),
            array(
                array('isRenewal' => false, 'birthday' => '2000-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                255,
            ),

            // Assurance Ski
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => true, 'city' => 'suresnes', 'family' => false),
                245,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '2000-01-01', 'insurance' => 1, 'insuranceIj' => 0, 'insuranceSki' => true, 'city' => 'suresnes', 'family' => false),
                230,
            ),

            // Assurance
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 0, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                233,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 2, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                243,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 3, 'insuranceIj' => 0, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                250,
            ),

            // Assurance IJ
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 1, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                258,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 2, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                270,
            ),
            array(
                array('isRenewal' => true, 'birthday' => '1980-01-01', 'insurance' => 1, 'insuranceIj' => 3, 'insuranceSki' => false, 'city' => 'suresnes', 'family' => false),
                275,
            ),
        );
    }
}
