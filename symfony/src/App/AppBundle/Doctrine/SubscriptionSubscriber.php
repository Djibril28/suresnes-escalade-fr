<?php

namespace App\AppBundle\Doctrine;

use App\AppBundle\Entity\Subscription;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\AppBundle\Event\SubscriptionEvent;
use App\AppBundle\AppEvents;

class SubscriptionSubscriber implements EventSubscriber
{
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
        );
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $subscription = $args->getEntity();

        if ($subscription instanceof Subscription) {
            $event = new SubscriptionEvent($subscription);
            $this->dispatcher->dispatch(AppEvents::SUBSCRIPTION_UPDATED, $event);

            if ($args->hasChangedField('status') && $subscription->isActive()) {
                $this->dispatcher->dispatch(AppEvents::SUBSCRIPTION_VALIDATED, $event);
            }

            if ($args->hasChangedField('status') && $subscription->isInactive()) {
                $this->dispatcher->dispatch(AppEvents::SUBSCRIPTION_CANCELLED, $event);
            }
        }
    }
}