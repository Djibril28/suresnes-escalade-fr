<?php

namespace App\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Subscription
 *
 * @ORM\Table(name="wp_app_subscription", indexes={
 *     @ORM\Index(name="ix_status", columns={"status"})
 * })
 * @ORM\Entity(repositoryClass="App\AppBundle\Entity\SubscriptionRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @Assert\Callback(methods={"validateSlots"})
 * @Assert\Callback(methods={"validateCertificate"})
 * @Assert\Callback(methods={"validateComplete"})
 */
class Subscription {
    const STATUS_WAITING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = -1;

    const CERTIFICATE_ABSENT = 0;
    const CERTIFICATE_WAITING = 1;
    const CERTIFICATE_VALID = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;
    private $user;

    /**
     * @var Season
     * @ORM\ManyToOne(targetEntity="Season", cascade={"all"}, fetch="EAGER")
     * @Assert\NotBlank()
     */
    private $season;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_last_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"parent"})
     */
    private $parentLastName;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_first_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"parent"})
     */
    private $parentFirstName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var float
     *
     * @ORM\Column(name="age", type="float", nullable=true)
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=1, nullable=true)
     * @Assert\NotBlank()
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=500, nullable=true)
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=10, nullable=true)
     * @Assert\NotBlank()
     */
    private $postalCode = '92150';

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $city = 'Suresnes';

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_phone", type="string", length=10, nullable=true)
     */
    private $parentPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_email", type="string", length=255, nullable=true)
     */
    private $parentEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="climbing_level", type="string", length=20, nullable=true)
     * @Assert\NotBlank()
     */
    private $climbingLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="climbing_experience", type="string", length=20, nullable=true)
     * @Assert\NotBlank()
     */
    private $climbingExperience;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_ffme", type="string", length=20, nullable=true)
     * @Assert\NotBlank()
     */
    private $passportFfme;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_competitor", type="integer")
     */
    private $isCompetitor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = self::STATUS_WAITING;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_renewal", type="boolean")
     */
    private $isRenewal = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activated_at", type="datetime", nullable=true)
     */
    private $activatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="activated_by", type="string", length=255, nullable=true)
     */
    private $activatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="occupation", type="string", length=255, nullable=true)
     */
    private $occupation;

    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=2, nullable=true, options={"default" = "FR"})
     */
    private $nationality = 'FR';

    /**
     * @var string
     *
     * @ORM\Column(name="licence", type="string", length=10, nullable=true)
     * @Assert\Range(min=0, max=1000000)
     */
    private $licence;

    /**
     * @var bool
     *
     * @ORM\Column(name="licence_family", type="boolean")
     */
    private $licenceFamily = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="licence_date", type="date", nullable=true)
     */
    private $licenceDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="no_slot", type="boolean")
     */
    private $noSlot = false;

    /**
     * @var Slot
     * @ORM\ManyToOne(targetEntity="Slot", fetch="EAGER", cascade={})
     * @Assert\NotBlank(groups={"slots"})
     */
    private $slot1;

    /**
     * @var Slot
     * @ORM\ManyToOne(targetEntity="Slot", fetch="EAGER", cascade={})
     * @Assert\NotBlank(groups={"slots"})
     */
    private $slot2;

    /**
     * @var Slot
     * @ORM\ManyToOne(targetEntity="Slot", fetch="EAGER", inversedBy="subscriptions", cascade={})
     */
    private $activatedSlot;

    /**
     * @var string
     *
     * @ORM\Column(name="certificate_file", type="string", length=255, nullable=true)
     * @Assert\File()
     */
    private $certificateFile;

    /**
     * @var string
     *
     * @ORM\Column(name="certificate_doctor", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"certificate"})
     */
    private $certificateDoctor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="certificate_date", type="datetime", nullable=true)
     * @Assert\Date()
     */
    private $certificateDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="certificate_competition", type="boolean")
     */
    private $certificateCompetition = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="certification_status", type="integer")
     */
    private $certificateStatus = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="insurance", type="integer")
     */
    private $insurance = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="insurance_ij", type="integer")
     */
    private $insuranceIj = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="insurance_slackline", type="integer")
     */
    private $insuranceSlackline = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="insurance_ski", type="integer")
     */
    private $insuranceSki = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_3_times", type="boolean")
     */
    private $payment3Times = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_pass92", type="boolean")
     */
    private $paymentPass92 = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_coupon_sport", type="boolean")
     */
    private $paymentCouponSport = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_subvension_ce", type="boolean")
     */
    private $paymentSubvensionCe = false;

    /**
     * @var float
     *
     * @ORM\Column(name="price_calculated", type="float")
     */
    private $priceCalculated = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="price_corrected", type="float")
     */
    private $priceCorrected = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_total", type="float")
     */
    private $paidTotal = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_cash", type="float")
     */
    private $paidCash = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_cb", type="float")
     */
    private $paidCB = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_pass92", type="float")
     */
    private $paidPass92 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_coupon_sport", type="float")
     */
    private $paidCouponSport = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_subvension_ce", type="float")
     */
    private $paidSubvensionCe = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_regul", type="float")
     */
    private $paidRegul = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_check01", type="float")
     */
    private $paidCheck01 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_check02", type="float")
     */
    private $paidCheck02 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_check03", type="float")
     */
    private $paidCheck03 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_check_garantee", type="float")
     */
    private $paidCheckGarantee = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="secret_key", type="string", length=50, nullable=true)
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    public function __construct() {
        $this->createdAt = new \DateTime();
        $this->birthday = \DateTime::createFromFormat('Y-m-d', '1980-01-01');
        $this->certificateDate = new \DateTime();
        $this->key = md5(uniqid('se', false));
    }

    public static function getClimbingLevels() {
        return array(
            'debutant' => 'Débutant (n\'a jamais pratiqué)',
            'debrouille' => 'Débrouillé (sait grimper en moulinette)',
            'moyen' => 'Moyen (sait grimper en tête et en moulinette)',
            'bon' => 'Bon (sait grimper en tête du 6a à vue)',
            'tresbon' => 'Très Bon (sait grimper en tête du 7a et plus à vue)',
            'BE' => 'B.E. (sait regarder les autres grimper)',
        );
    }

    public static function getClimbingExperiences() {
        return array(
            'aucun' => 'Aucune, je suis débutant',
            'unpeu' => 'Moins d\'une année',
            '1an' => 'J\'en ai déjà fait durant 1 an',
            '2ans' => 'J\'en ai déjà fait durant 2 ans',
            '3ans' => 'J\'en ai déjà fait durant 3 ans',
            '4ans' => 'J\'en ai déjà fait durant 4 ans',
            '5ans_et_plus' => 'J\'en ai déjà fait durant 5 ans ou plus',
        );
    }

    public static function getPassportsFFME() {
        return array(
            'aucun' => 'Je ne possède malheureusement aucun Passeport',
            'premier_pas' => 'Passeport "Premier Pas"',
            'blanc' => 'Passeport Blanc',
            'jaune' => 'Passeport Jaune',
            'orange' => 'Passeport Orange',
            'vert' => 'Passeport Vert',
            'bleu' => 'Passeport Bleu',
            'violet' => 'Passeport Violet',
            'rouge' => 'Passeport Rouge',
            'noir' => 'Passeport Noir',
        );
    }

    public static function getNationalityChoices() {
        static $nationalities = array(
            'AF' => 'Afghanistan',
            'ZA' => 'Afrique du Sud',
            'AL' => 'Albanie',
            'DZ' => 'Algérie',
            'DE' => 'Allemagne',
            'AD' => 'Andorre',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctique',
            'AG' => 'Antigua-et-Barbuda',
            'AN' => 'Antilles néerlandaises',
            'SA' => 'Arabie saoudite',
            'AR' => 'Argentine',
            'AM' => 'Arménie',
            'AW' => 'Aruba',
            'AU' => 'Australie',
            'AT' => 'Autriche',
            'AZ' => 'Azerbaïdjan',
            'BS' => 'Bahamas',
            'BH' => 'Bahreïn',
            'BD' => 'Bangladesh',
            'BB' => 'Barbade',
            'PW' => 'Belau',
            'BE' => 'Belgique',
            'BZ' => 'Belize',
            'BJ' => 'Bénin',
            'BM' => 'Bermudes',
            'BT' => 'Bhoutan',
            'BY' => 'Biélorussie',
            'MM' => 'Birmanie',
            'BO' => 'Bolivie',
            'BA' => 'Bosnie-Herzégovine',
            'BW' => 'Botswana',
            'BR' => 'Brésil',
            'BN' => 'Brunei',
            'BG' => 'Bulgarie',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodge',
            'CM' => 'Cameroun',
            'CA' => 'Canada',
            'CV' => 'Cap-Vert',
            'CL' => 'Chili',
            'CN' => 'Chine',
            'CY' => 'Chypre',
            'CO' => 'Colombie',
            'KM' => 'Comores',
            'CG' => 'Congo',
            'KP' => 'Corée du Nord',
            'KR' => 'Corée du Sud',
            'CR' => 'Costa Rica',
            'CI' => 'Côte d\'Ivoire',
            'HR' => 'Croatie',
            'CU' => 'Cuba',
            'DK' => 'Danemark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominique',
            'EG' => 'Égypte',
            'AE' => 'Émirats arabes unis',
            'EC' => 'Équateur',
            'ER' => 'Érythrée',
            'ES' => 'Espagne',
            'EE' => 'Estonie',
            'US' => 'États-Unis',
            'ET' => 'Éthiopie',
            'MK' => 'ex-République yougoslave de Macédoine',
            'FI' => 'Finlande',
            'FR' => 'France',
            'GA' => 'Gabon',
            'GM' => 'Gambie',
            'GE' => 'Géorgie',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Grèce',
            'GD' => 'Grenade',
            'GL' => 'Groenland',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GN' => 'Guinée',
            'GQ' => 'Guinée équatoriale',
            'GW' => 'Guinée-Bissao',
            'GY' => 'Guyana',
            'GF' => 'Guyane française',
            'HT' => 'Haïti',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hongrie',
            'BV' => 'Ile Bouvet',
            'CX' => 'Ile Christmas',
            'NF' => 'Ile Norfolk',
            'KY' => 'Iles Cayman',
            'CK' => 'Iles Cook',
            'CC' => 'Iles des Cocos (Keeling)',
            'FK' => 'Iles Falkland',
            'FO' => 'Iles Féroé',
            'FJ' => 'Iles Fidji',
            'GS' => 'Iles Géorgie du Sud et Sandwich du Sud',
            'HM' => 'Iles Heard et McDonald',
            'MH' => 'Iles Marshall',
            'UM' => 'Iles mineures éloignées des États-Unis',
            'PN' => 'Iles Pitcairn',
            'SB' => 'Iles Salomon',
            'SJ' => 'Iles Svalbard et Jan Mayen',
            'TC' => 'Iles Turks-et-Caicos',
            'VI' => 'Iles Vierges américaines',
            'VG' => 'Iles Vierges britanniques',
            'IN' => 'Inde',
            'ID' => 'Indonésie',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Irlande',
            'IS' => 'Islande',
            'IL' => 'Israël',
            'IT' => 'Italie',
            'JM' => 'Jamaïque',
            'JP' => 'Japon',
            'JO' => 'Jordanie',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KG' => 'Kirghizistan',
            'KI' => 'Kiribati',
            'KW' => 'Koweït',
            'LA' => 'Laos',
            'LS' => 'Lesotho',
            'LV' => 'Lettonie',
            'LB' => 'Liban',
            'LR' => 'Liberia',
            'LY' => 'Libye',
            'LI' => 'Liechtenstein',
            'LT' => 'Lituanie',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macédoine',
            'MG' => 'Madagascar',
            'MY' => 'Malaisie',
            'MW' => 'Malawi',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malte',
            'MP' => 'Mariannes du Nord',
            'MA' => 'Maroc',
            'MQ' => 'Martinique',
            'MU' => 'Maurice',
            'MR' => 'Mauritanie',
            'YT' => 'Mayotte',
            'MX' => 'Mexique',
            'FM' => 'Micronésie',
            'MD' => 'Moldavie',
            'MC' => 'Monaco',
            'MN' => 'Mongolie',
            'MS' => 'Montserrat',
            'MZ' => 'Mozambique',
            'NA' => 'Namibie',
            'NR' => 'Nauru',
            'NP' => 'Népal',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Nioué',
            'NO' => 'Norvège',
            'NC' => 'Nouvelle-Calédonie',
            'NZ' => 'Nouvelle-Zélande',
            'OM' => 'Oman',
            'UG' => 'Ouganda',
            'UZ' => 'Ouzbékistan',
            'PK' => 'Pakistan',
            'PA' => 'Panama',
            'PG' => 'Papouasie-Nouvelle-Guinée',
            'PY' => 'Paraguay',
            'NL' => 'Pays-Bas',
            'PE' => 'Pérou',
            'PH' => 'Philippines',
            'PL' => 'Pologne',
            'PF' => 'Polynésie française',
            'PR' => 'Porto Rico',
            'PT' => 'Portugal',
            'QA' => 'Qatar',
            'CF' => 'République centrafricaine',
            'CD' => 'République démocratique du Congo',
            'DO' => 'République dominicaine',
            'CZ' => 'République tchèque',
            'RE' => 'Réunion',
            'RO' => 'Roumanie',
            'GB' => 'Royaume-Uni',
            'RU' => 'Russie',
            'RW' => 'Rwanda',
            'EH' => 'Sahara occidental',
            'KN' => 'Saint-Christophe-et-Niévès',
            'SM' => 'Saint-Marin',
            'PM' => 'Saint-Pierre-et-Miquelon',
            'VA' => 'Saint-Siège',
            'VC' => 'Saint-Vincent-et-les-Grenadines',
            'SH' => 'Sainte-Hélène',
            'LC' => 'Sainte-Lucie',
            'SV' => 'Salvador',
            'WS' => 'Samoa',
            'AS' => 'Samoa américaines',
            'ST' => 'Sao Tomé-et-Principe',
            'SN' => 'Sénégal',
            'ER' => 'Serbie ',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapour',
            'SK' => 'Slovaquie',
            'SI' => 'Slovénie',
            'SO' => 'Somalie',
            'SD' => 'Soudan',
            'LK' => 'Sri Lanka',
            'SE' => 'Suède',
            'CH' => 'Suisse',
            'SR' => 'Suriname',
            'SZ' => 'Swaziland',
            'SY' => 'Syrie',
            'TJ' => 'Tadjikistan',
            'TW' => 'Taïwan',
            'TZ' => 'Tanzanie',
            'TD' => 'Tchad',
            'TF' => 'Terres australes françaises',
            'IO' => 'Territoire britannique de l\'Océan Indien',
            'TH' => 'Thaïlande',
            'TL' => 'Timor Oriental',
            'TG' => 'Togo',
            'TK' => 'Tokélaou',
            'TO' => 'Tonga',
            'TT' => 'Trinité-et-Tobago',
            'TN' => 'Tunisie',
            'TM' => 'Turkménistan',
            'TR' => 'Turquie',
            'TV' => 'Tuvalu',
            'UA' => 'Ukraine',
            'UY' => 'Uruguay',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viêt Nam',
            'WF' => 'Wallis-et-Futuna',
            'YE' => 'Yémen',
            'ZM' => 'Zambie',
            'ZW' => 'Zimbabwe ZW',
        );

        return $nationalities;
    }

    public static function getIsCompetitorChoices() {
        return array(
            0 => 'Non, les compétitions ne m\'interressent pas',
            1 => 'Après tout, pourquoi pas !',
            2 => 'Oui, les compétitions m\'interressent',
        );
    }

    public static function getStatusChoices() {
        return array(
            self::STATUS_WAITING => 'En Attente',
            self::STATUS_ACTIVE => 'Actif',
            self::STATUS_INACTIVE => 'Inactif',
        );
    }

    public static function getCertificateStatusChoices() {
        return array(
            self::CERTIFICATE_ABSENT => 'Non-transmis',
            self::CERTIFICATE_WAITING => 'Non-validé',
            self::CERTIFICATE_VALID => 'Validé',
        );
    }

    /**
     * @ORM\PreFlush
     */
    public function calculatePriceAndPaid() {
        $this->setPriceCalculated($this->season->getSubscriptionPrice($this, false));
        $this->paidTotal = $this->getPaidTotal();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set season
     *
     * @param integer $season
     * @return Subscription
     */
    public function setSeason($season) {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return integer
     */
    public function getSeason() {
        return $this->season;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Subscription
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Subscription
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return Subscription
     */
    public function setSex($sex) {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex() {
        return $this->sex;
    }

    /**
     * Set isRenewal
     *
     * @param boolean $isRenewal
     * @return Subscription
     */
    public function setIsRenewal($isRenewal) {
        $this->isRenewal = (bool) $isRenewal;

        return $this;
    }

    /**
     * Get isRenewal
     *
     * @return boolean
     */
    public function getIsRenewal() {
        return $this->isRenewal;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Subscription
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set activatedAt
     *
     * @param \DateTime $activatedAt
     * @return Subscription
     */
    public function setActivatedAt($activatedAt) {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    /**
     * Get activatedAt
     *
     * @return \DateTime
     */
    public function getActivatedAt() {
        return $this->activatedAt;
    }

    /**
     * Set activatedBy
     *
     * @param string $activatedBy
     * @return Subscription
     */
    public function setActivatedBy($activatedBy) {
        $this->activatedBy = $activatedBy;

        return $this;
    }

    /**
     * Get activatedBy
     *
     * @return string
     */
    public function getActivatedBy() {
        return $this->activatedBy;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Subscription
     */
    public function setBirthday($birthday) {
        $this->birthday = $birthday;

        $this->age = (int) $this->season->getEndAt()->diff($birthday)->format('%Y');

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday() {
        return $this->birthday;
    }

    /**
     * Set age
     *
     * @param float $age
     * @return Subscription
     */
    public function setAge($age) {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return float
     */
    public function getAge() {
        return $this->age;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     * @return Subscription
     */
    public function setOccupation($occupation) {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string
     */
    public function getOccupation() {
        return $this->occupation;
    }

    /**
     * Set licence
     *
     * @param string $licence
     * @return Subscription
     */
    public function setLicence($licence) {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return string
     */
    public function getLicence() {
        return $this->licence;
    }

    /**
     * Set certificateFile
     *
     * @param string $certificateFile
     * @return Subscription
     */
    public function setCertificateFile($certificateFile) {
        $this->certificateFile = $certificateFile;

        return $this;
    }

    /**
     * Get certificateFile
     *
     * @return string
     */
    public function getCertificateFile() {
        return $this->certificateFile;
    }

    /**
     * Set certificateDoctor
     *
     * @param string $certificateDoctor
     * @return Subscription
     */
    public function setCertificateDoctor($certificateDoctor) {
        $this->certificateDoctor = $certificateDoctor;

        return $this;
    }

    /**
     * Get certificateDoctor
     *
     * @return string
     */
    public function getCertificateDoctor() {
        return $this->certificateDoctor;
    }

    /**
     * Set certificateDate
     *
     * @param \DateTime $certificateDate
     * @return Subscription
     */
    public function setCertificateDate($certificateDate) {
        $this->certificateDate = $certificateDate;

        return $this;
    }

    /**
     * Get certificateDate
     *
     * @return \DateTime
     */
    public function getCertificateDate() {
        return $this->certificateDate;
    }

    public function __toString() {
        return sprintf('%s %s (%s)', $this->firstName, $this->lastName, $this->season ?  : '');
    }

    /**
     * Set userId
     *
     * @param int
     * @return Subscription
     */
    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set slot1
     *
     * @param \App\AppBundle\Entity\Slot $slot1
     * @return Subscription
     */
    public function setSlot1(\App\AppBundle\Entity\Slot $slot1 = null) {
        $this->slot1 = $slot1;

        return $this;
    }

    /**
     * Get slot1
     *
     * @return \App\AppBundle\Entity\Slot
     */
    public function getSlot1() {
        return $this->slot1;
    }

    /**
     * Set slot2
     *
     * @param \App\AppBundle\Entity\Slot $slot2
     * @return Subscription
     */
    public function setSlot2(\App\AppBundle\Entity\Slot $slot2 = null) {
        $this->slot2 = $slot2;

        return $this;
    }

    /**
     * Get slot2
     *
     * @return \App\AppBundle\Entity\Slot
     */
    public function getSlot2() {
        return $this->slot2;
    }

    /**
     * Set activatedSlot
     *
     * @param \App\AppBundle\Entity\Slot $activatedSlot
     * @return Subscription
     */
    public function setActivatedSlot(\App\AppBundle\Entity\Slot $activatedSlot = null) {
        $this->activatedSlot = $activatedSlot;

        return $this;
    }

    /**
     * Get activatedSlot
     *
     * @return \App\AppBundle\Entity\Slot
     */
    public function getActivatedSlot() {
        return $this->activatedSlot;
    }

    private function filterPhone($value) {
        return preg_replace('/[^0-9]/', '', $value);
    }

    // Validation

    public function validateSlots(ExecutionContextInterface $context) {
        if ($this->noSlot) {
            $this->slot1 = $this->slot2 = null;
        } else if (null === $this->slot1 || null === $this->slot2) {
            $context->addViolation('Vous devez choisir 2 créneaux différents', array(), null);
        } else if ($this->slot1->getId() === $this->slot2->getId()) {
            $context->addViolation('Vous devez choisir 2 créneaux différents', array(), null);
        }
    }

    public function validateCertificate(ExecutionContextInterface $context) {
        if (null !== $this->certificateDate) {
            $minDate = clone $this->season->getBeginAt();
            $minDate->modify('-9 months');
            $maxDate = new \DateTime();

            if ($this->certificateDate > $maxDate) {
                $context->addViolation('La date du certificat médical est dans le futur.', array(), null);
            } elseif ($this->certificateDate < $minDate) {
                $context->addViolation('La date du certificat médical est trop ancienne (1 an au 1er septembre).', array(), null);
            }
        }

        if (self::CERTIFICATE_VALID !== $this->certificateStatus) {
            return;
        }

        if (null === $this->certificateDoctor) {
            $context->addViolation('Le nom du médecin doit être saisi pour valider le certificat médical', array(), null);
        }

        if (null === $this->certificateDate) {
            $context->addViolation('La date doit être saisie pour valider le certificat médical', array(), null);
        }
    }

    public function validateComplete(ExecutionContextInterface $context) {
        if (self::STATUS_ACTIVE !== $this->status) {
            return;
        }

        if ($this->noSlot) {
            $this->activatedSlot = null;
        } else if (null === $this->activatedSlot) {
            $context->addViolation('Vous devez choisir un créneau pour valider une inscription', array(), null);
        }

        if (self::CERTIFICATE_VALID !== $this->certificateStatus) {
            $context->addViolation('Le certificat médical doit être validé pour valider une inscription', array(), null);
        }

        if ($this->getPaidTotal() < $this->getPriceCorrected()) {
            $context->addViolation('Le montant total payé est insuffisant pour valider une inscription', array(), null);
        }
    }

    // Upload

    public function getCertificateFilePath() {
        return null === $this->certificateFile ? null : $this->getUploadRootDir() . $this->certificateFile;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return APP_ROOT_DIR . '/certifs/' . md5($this->getId()) . '/';
    }

    protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return APP_ROOT_DIR . '/certifs/tmp/';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadCertificateFile() {
        if (null === $this->certificateFile) {
            return;
        }

        return;

        if (!$this->id) {
            $this->certificateFile->move($this->getTmpUploadRootDir(), $this->certificateFile->getClientOriginalName());
        } else {
            $this->certificateFile->move($this->getUploadRootDir(), $this->certificateFile->getClientOriginalName());
        }
        $this->setCertificateFile($this->certificateFile->getClientOriginalName());
    }

    /**
     * @ORM\PostPersist()
     */
    public function moveCertificateFile() {
        if (null === $this->certificateFile) {
            return;
        }

        if (!is_dir($this->getUploadRootDir())) {
            mkdir($this->getUploadRootDir());
        }

        copy($this->getTmpUploadRootDir() . $this->certificateFile, $this->getCertificateFilePath());
        unlink($this->getTmpUploadRootDir() . $this->certificateFile);
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeCertificateFile() {
        // if ($this->certificateFile) {
        //     $file = $this->getCertificateFilePath();
        //     @unlink($file);
        //     @rmdir($this->getUploadRootDir());
        // }
    }

    /**
     * Set certificateCompetition
     *
     * @param boolean $certificateCompetition
     * @return Subscription
     */
    public function setCertificateCompetition($certificateCompetition) {
        $this->certificateCompetition = $certificateCompetition;

        return $this;
    }

    /**
     * Get certificateCompetition
     *
     * @return boolean
     */
    public function getCertificateCompetition() {
        return $this->certificateCompetition;
    }

    /**
     * Set noSlot
     *
     * @param boolean $noSlot
     * @return Subscription
     */
    public function setNoSlot($noSlot) {
        $this->noSlot = $noSlot;

        return $this;
    }

    /**
     * Get noSlot
     *
     * @return boolean
     */
    public function getNoSlot() {
        return $this->noSlot;
    }

    /**
     * Set insurance
     *
     * @param integer $insurance
     * @return Subscription
     */
    public function setInsurance($insurance) {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return integer
     */
    public function getInsurance() {
        return $this->insurance;
    }

    /**
     * Set insuranceIj
     *
     * @param integer $insuranceIj
     * @return Subscription
     */
    public function setInsuranceIj($insuranceIj) {
        $this->insuranceIj = $insuranceIj;

        return $this;
    }

    /**
     * Get insuranceIj
     *
     * @return integer
     */
    public function getInsuranceIj() {
        return $this->insuranceIj;
    }

    /**
     * Set insuranceSlackline
     *
     * @param boolean $insuranceSlackline
     * @return Subscription
     */
    public function setInsuranceSlackline($insuranceSlackline) {
        $this->insuranceSlackline = $insuranceSlackline;

        return $this;
    }

    /**
     * Get insuranceSlackline
     *
     * @return boolean
     */
    public function getInsuranceSlackline() {
        return $this->insuranceSlackline;
    }

    /**
     * Set insuranceSki
     *
     * @param boolean $insuranceSki
     * @return Subscription
     */
    public function setInsuranceSki($insuranceSki) {
        $this->insuranceSki = $insuranceSki;

        return $this;
    }

    /**
     * Get insuranceSki
     *
     * @return boolean
     */
    public function getInsuranceSki() {
        return $this->insuranceSki;
    }

    /**
     * Set payment3Times
     *
     * @param boolean $payment3Times
     * @return Subscription
     */
    public function setPayment3Times($payment3Times) {
        $this->payment3Times = $payment3Times;

        return $this;
    }

    /**
     * Get payment3Times
     *
     * @return boolean
     */
    public function getPayment3Times() {
        return $this->payment3Times;
    }

    /**
     * Set parentLastName
     *
     * @param string $parentLastName
     * @return Subscription
     */
    public function setParentLastName($parentLastName) {
        $this->parentLastName = $parentLastName;

        return $this;
    }

    /**
     * Get parentLastName
     *
     * @return string
     */
    public function getParentLastName() {
        return $this->parentLastName;
    }

    /**
     * Set parentFirstName
     *
     * @param string $parentFirstName
     * @return Subscription
     */
    public function setParentFirstName($parentFirstName) {
        $this->parentFirstName = $parentFirstName;

        return $this;
    }

    /**
     * Get parentFirstName
     *
     * @return string
     */
    public function getParentFirstName() {
        return $this->parentFirstName;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Subscription
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Subscription
     */
    public function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode() {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Subscription
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Subscription
     */
    public function setPhone($phone) {
        $this->phone = $this->filterPhone($phone);

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set parentPhone
     *
     * @param string $parentPhone
     * @return Subscription
     */
    public function setParentPhone($parentPhone) {
        $this->parentPhone = $this->filterPhone($parentPhone);

        return $this;
    }

    /**
     * Get parentPhone
     *
     * @return string
     */
    public function getParentPhone() {
        return $this->parentPhone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Subscription
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set climbingLevel
     *
     * @param string $climbingLevel
     * @return Subscription
     */
    public function setClimbingLevel($climbingLevel) {
        $this->climbingLevel = $climbingLevel;

        return $this;
    }

    /**
     * Get climbingLevel
     *
     * @return string
     */
    public function getClimbingLevel() {
        return $this->climbingLevel;
    }

    /**
     * Set climbingExperience
     *
     * @param string $climbingExperience
     * @return Subscription
     */
    public function setClimbingExperience($climbingExperience) {
        $this->climbingExperience = $climbingExperience;

        return $this;
    }

    /**
     * Get climbingExperience
     *
     * @return string
     */
    public function getClimbingExperience() {
        return $this->climbingExperience;
    }

    /**
     * Set passportFfme
     *
     * @param string $passportFfme
     * @return Subscription
     */
    public function setPassportFfme($passportFfme) {
        $this->passportFfme = $passportFfme;

        return $this;
    }

    /**
     * Get passportFfme
     *
     * @return string
     */
    public function getPassportFfme() {
        return $this->passportFfme;
    }

    /**
     * Set isCompetitor
     *
     * @param boolean $isCompetitor
     * @return Subscription
     */
    public function setIsCompetitor($isCompetitor) {
        $this->isCompetitor = $isCompetitor;

        return $this;
    }

    /**
     * Get isCompetitor
     *
     * @return boolean
     */
    public function getIsCompetitor() {
        return $this->isCompetitor;
    }

    /**
     * Set parentEmail
     *
     * @param string $parentEmail
     * @return Subscription
     */
    public function setParentEmail($parentEmail) {
        $this->parentEmail = $parentEmail;

        return $this;
    }

    /**
     * Get parentEmail
     *
     * @return string
     */
    public function getParentEmail() {
        return $this->parentEmail;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Subscription
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @return  bool
     */
    public function isActive() {
        return self::STATUS_ACTIVE === $this->status;
    }

    /**
     * @return  bool
     */
    public function isInactive() {
        return self::STATUS_INACTIVE === $this->status;
    }

    /**
     * Set certificateStatus
     *
     * @param integer $certificateStatus
     * @return Subscription
     */
    public function setCertificateStatus($certificateStatus) {
        $this->certificateStatus = $certificateStatus;

        return $this;
    }

    /**
     * Get certificateStatus
     *
     * @return integer
     */
    public function getCertificateStatus() {
        return $this->certificateStatus;
    }

    /**
     * Set paymentPass92
     *
     * @param boolean $paymentPass92
     * @return Subscription
     */
    public function setPaymentPass92($paymentPass92) {
        $this->paymentPass92 = $paymentPass92;

        return $this;
    }

    /**
     * Get paymentPass92
     *
     * @return boolean
     */
    public function getPaymentPass92() {
        return $this->paymentPass92;
    }

    /**
     * Set paymentCouponSport
     *
     * @param boolean $paymentCouponSport
     * @return Subscription
     */
    public function setPaymentCouponSport($paymentCouponSport) {
        $this->paymentCouponSport = $paymentCouponSport;

        return $this;
    }

    /**
     * Get paymentCouponSport
     *
     * @return boolean
     */
    public function getPaymentCouponSport() {
        return $this->paymentCouponSport;
    }

    /**
     * Set paymentSubvensionCe
     *
     * @param boolean $paymentSubvensionCe
     * @return Subscription
     */
    public function setPaymentSubvensionCe($paymentSubvensionCe) {
        $this->paymentSubvensionCe = $paymentSubvensionCe;

        return $this;
    }

    /**
     * Get paymentSubvensionCe
     *
     * @return boolean
     */
    public function getPaymentSubvensionCe() {
        return $this->paymentSubvensionCe;
    }

    /**
     * Set priceCalculated
     *
     * @param float $priceCalculated
     * @return Subscription
     */
    public function setPriceCalculated($priceCalculated) {
        $this->priceCalculated = $priceCalculated;

        return $this;
    }

    /**
     * Get priceCalculated
     *
     * @return float
     */
    public function getPriceCalculated() {
        return $this->priceCalculated;
    }

    /**
     * Set priceCorrected
     *
     * @param float $priceCorrected
     * @return Subscription
     */
    public function setPriceCorrected($priceCorrected) {
        $this->priceCorrected = $priceCorrected;

        return $this;
    }

    /**
     * Get priceCorrected
     *
     * @return float
     */
    public function getPriceCorrected() {
        return $this->priceCorrected;
    }

    public function getPrice() {
        return $this->getPriceCorrected() ?  : $this->getPriceCalculated();
    }

    /**
     * Set paidTotal
     *
     * @param float $paidTotal
     * @return Subscription
     */
    public function setPaidTotal($paidTotal) {
        $this->paidTotal = $paidTotal;

        return $this;
    }

    /**
     * Get paidTotal
     *
     * @return float
     */
    public function getPaidTotal() {
        $this->paidTotal = 0
        +$this->paidCash
        +$this->paidCB
        +$this->paidPass92
        +$this->paidCouponSport
        +$this->paidSubvensionCe
        +$this->paidRegul
        +$this->paidCheck01
        +$this->paidCheck02
        +$this->paidCheck03
        +$this->paidCheckGarantee
        ;

        return $this->paidTotal;
    }

    /**
     * Set paidCash
     *
     * @param float $paidCash
     * @return Subscription
     */
    public function setPaidCash($paidCash) {
        $this->paidCash = $paidCash;

        return $this;
    }

    /**
     * Get paidCash
     *
     * @return float
     */
    public function getPaidCash() {
        return $this->paidCash;
    }

    /**
     * Set paidCB
     *
     * @param float $paidCB
     * @return Subscription
     */
    public function setPaidCB($paidCB) {
        $this->paidCB = $paidCB;

        return $this;
    }

    /**
     * Get paidCB
     *
     * @return float
     */
    public function getPaidCB() {
        return $this->paidCB;
    }

    /**
     * Set paidPass92
     *
     * @param float $paidPass92
     * @return Subscription
     */
    public function setPaidPass92($paidPass92) {
        $this->paidPass92 = $paidPass92;

        return $this;
    }

    /**
     * Get paidPass92
     *
     * @return float
     */
    public function getPaidPass92() {
        return $this->paidPass92;
    }

    /**
     * Set paidCouponSport
     *
     * @param float $paidCouponSport
     * @return Subscription
     */
    public function setPaidCouponSport($paidCouponSport) {
        $this->paidCouponSport = $paidCouponSport;

        return $this;
    }

    /**
     * Get paidCouponSport
     *
     * @return float
     */
    public function getPaidCouponSport() {
        return $this->paidCouponSport;
    }

    /**
     * Set paidSubvensionCe
     *
     * @param float $paidSubvensionCe
     * @return Subscription
     */
    public function setPaidSubvensionCe($paidSubvensionCe) {
        $this->paidSubvensionCe = $paidSubvensionCe;

        return $this;
    }

    /**
     * Get paidSubvensionCe
     *
     * @return float
     */
    public function getPaidSubvensionCe() {
        return $this->paidSubvensionCe;
    }

    /**
     * Set paidRegul
     *
     * @param float $paidRegul
     * @return Subscription
     */
    public function setPaidRegul($paidRegul) {
        $this->paidRegul = $paidRegul;

        return $this;
    }

    /**
     * Get paidRegul
     *
     * @return float
     */
    public function getPaidRegul() {
        return $this->paidRegul;
    }

    /**
     * Set paidCheck01
     *
     * @param float $paidCheck01
     * @return Subscription
     */
    public function setPaidCheck01($paidCheck01) {
        $this->paidCheck01 = $paidCheck01;

        return $this;
    }

    /**
     * Get paidCheck01
     *
     * @return float
     */
    public function getPaidCheck01() {
        return $this->paidCheck01;
    }

    /**
     * Set paidCheck02
     *
     * @param float $paidCheck02
     * @return Subscription
     */
    public function setPaidCheck02($paidCheck02) {
        $this->paidCheck02 = $paidCheck02;

        return $this;
    }

    /**
     * Get paidCheck02
     *
     * @return float
     */
    public function getPaidCheck02() {
        return $this->paidCheck02;
    }

    /**
     * Set paidCheck03
     *
     * @param float $paidCheck03
     * @return Subscription
     */
    public function setPaidCheck03($paidCheck03) {
        $this->paidCheck03 = $paidCheck03;

        return $this;
    }

    /**
     * Get paidCheck03
     *
     * @return float
     */
    public function getPaidCheck03() {
        return $this->paidCheck03;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return Subscription
     */
    public function setKey($key) {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * Set paidCheckGarantee
     *
     * @param float $paidCheckGarantee
     * @return Subscription
     */
    public function setPaidCheckGarantee($paidCheckGarantee) {
        $this->paidCheckGarantee = $paidCheckGarantee;

        return $this;
    }

    /**
     * Get paidCheckGarantee
     *
     * @return float
     */
    public function getPaidCheckGarantee() {
        return $this->paidCheckGarantee;
    }

    /**
     * Set licenceFamily
     *
     * @param boolean $licenceFamily
     * @return Subscription
     */
    public function setLicenceFamily($licenceFamily) {
        $this->licenceFamily = $licenceFamily;

        return $this;
    }

    /**
     * Get licenceFamily
     *
     * @return boolean
     */
    public function getLicenceFamily() {
        return $this->licenceFamily;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Subscription
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Set nationality
     *
     * @param string $nationality
     * @return Subscription
     */
    public function setNationality($nationality) {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get nationality
     *
     * @return string
     */
    public function getNationality() {
        return $this->nationality;
    }

    /**
     * Set licenceDate
     *
     * @param \DateTime $licenceDate
     * @return Subscription
     */
    public function setLicenceDate($licenceDate) {
        $this->licenceDate = $licenceDate;

        return $this;
    }

    /**
     * Get licenceDate
     *
     * @return \DateTime
     */
    public function getLicenceDate() {
        return $this->licenceDate;
    }
}
