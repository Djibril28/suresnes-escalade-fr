<?php

namespace App\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Slot
 *
 * @ORM\Table(name="wp_app_slot")
 * @ORM\Entity
 */
class Slot {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Season
	 * @ORM\ManyToOne(targetEntity="Season", cascade={"all"}, fetch="EAGER", inversedBy="slots")
	 * @Assert\NotBlank()
	 */
	private $season;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=64, nullable=false)
	 * @Assert\NotBlank()
	 */
	private $title;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $description;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", columnDefinition="TINYINT", nullable=false)
	 * @Assert\Range(min=1, max=7)
	 */
	private $dayOfWeek;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="time", nullable=false)
	 * @Assert\Time()
	 */
	private $beginTime;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="time", nullable=false)
	 * @Assert\Time()
	 */
	private $endTime;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 * @Assert\Date()
	 */
	private $minBirth;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 * @Assert\Date()
	 */
	private $maxBirth;

	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @Assert\NotBlank()
	 */
	private $maxPlaces = 20;

	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @Assert\NotBlank()
	 */
	private $marginPlaces = 2;

	/**
	 * @ORM\OneToMany(targetEntity="Subscription", mappedBy="activatedSlot", cascade={}, orphanRemoval=false)
	 * @ORM\OrderBy({"status" = "DESC", "firstName" = "ASC", "lastName" = "ASC"})
	 */
	private $subscriptions;

	/**
	 * @ORM\OneToMany(targetEntity="Subscription", mappedBy="slot1", cascade={}, orphanRemoval=false)
	 * @ORM\OrderBy({"status" = "DESC", "createdAt" = "ASC"})
	 */
	private $waitingSubscriptions;

	/**
	 * @var Season
	 * @ORM\ManyToOne(targetEntity="SlotType", cascade={"all"}, fetch="EAGER")
	 */
	private $type;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_full", type="boolean")
	 */
	private $isFull;

	public static function getDayChoices() {
		return array(
			1 => 'Lundi',
			2 => 'Mardi',
			3 => 'Mercredi',
			4 => 'Jeudi',
			5 => 'Vendredi',
			6 => 'Samedi',
			7 => 'Dimanche',
		);
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->subscriptions = new ArrayCollection();
	}

	/**
	 * @return  string
	 */
	public function __toString() {
		$label = sprintf('%s / %s', $this->season, $this->title);

		if ($this->isFull) {
			$label = '[COMPLET] '.$label;
		}

		return $label;
	}

	/**
	 * @return  string
	 */
	public function getLabel() {
		$label = $this->title;

		if ($this->isFull) {
			$label = '[COMPLET] '.$label;
		}

		return $label;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Slot
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set dayOfWeek
	 *
	 * @param integer $dayOfWeek
	 * @return Slot
	 */
	public function setDayOfWeek($dayOfWeek) {
		$this->dayOfWeek = $dayOfWeek;

		return $this;
	}

	/**
	 * Get dayOfWeek
	 *
	 * @return integer
	 */
	public function getDayOfWeek() {
		return $this->dayOfWeek;
	}

	public function getReadableDayOfWeek() {
		switch ($this->dayOfWeek) {
			case 1:return 'Lundi';
			case 2:return 'Mardi';
			case 3:return 'Mercredi';
			case 4:return 'Jeudi';
			case 5:return 'Vendredi';
			case 6:return 'Samedi';
			case 7:return 'Dimanche';
		}
		throw new \LogicException('Incorrect day number: ' . $this->dayOfWeek);
	}

	/**
	 * Set beginTime
	 *
	 * @param \DateTime $beginTime
	 * @return Slot
	 */
	public function setBeginTime($beginTime) {
		$this->beginTime = $beginTime;

		return $this;
	}

	/**
	 * Get beginTime
	 *
	 * @return \DateTime
	 */
	public function getBeginTime() {
		return $this->beginTime;
	}

	/**
	 * Set endTime
	 *
	 * @param \DateTime $endTime
	 * @return Slot
	 */
	public function setEndTime($endTime) {
		$this->endTime = $endTime;

		return $this;
	}

	/**
	 * Get endTime
	 *
	 * @return \DateTime
	 */
	public function getEndTime() {
		return $this->endTime;
	}

	/**
	 * Set minBirth
	 *
	 * @param \DateTime $minBirth
	 * @return Slot
	 */
	public function setMinBirth($minBirth) {
		$this->minBirth = $minBirth;

		return $this;
	}

	/**
	 * Get minBirth
	 *
	 * @return \DateTime
	 */
	public function getMinBirth() {
		return $this->minBirth;
	}

	/**
	 * Set maxBirth
	 *
	 * @param \DateTime $maxBirth
	 * @return Slot
	 */
	public function setMaxBirth($maxBirth) {
		$this->maxBirth = $maxBirth;

		return $this;
	}

	/**
	 * Get maxBirth
	 *
	 * @return \DateTime
	 */
	public function getMaxBirth() {
		return $this->maxBirth;
	}

	/**
	 * Set season
	 *
	 * @param \App\AppBundle\Entity\Season $season
	 * @return Slot
	 */
	public function setSeason(\App\AppBundle\Entity\Season $season = null) {
		$this->season = $season;

		return $this;
	}

	/**
	 * Get season
	 *
	 * @return \App\AppBundle\Entity\Season
	 */
	public function getSeason() {
		return $this->season;
	}

	/**
	 * Set maxPlaces
	 *
	 * @param integer $maxPlaces
	 * @return Slot
	 */
	public function setMaxPlaces($maxPlaces) {
		$this->maxPlaces = $maxPlaces;

		return $this;
	}

	/**
	 * Get maxPlaces
	 *
	 * @return integer
	 */
	public function getMaxPlaces() {
		return $this->maxPlaces;
	}

	/**
	 * Set marginPlaces
	 *
	 * @param integer $marginPlaces
	 * @return Slot
	 */
	public function setMarginPlaces($marginPlaces) {
		$this->marginPlaces = $marginPlaces;

		return $this;
	}

	/**
	 * Get marginPlaces
	 *
	 * @return integer
	 */
	public function getMarginPlaces() {
		return $this->marginPlaces;
	}

	/**
	 * Add subscriptions
	 *
	 * @param \App\AppBundle\Entity\Subscription $subscriptions
	 * @return Slot
	 */
	public function addSubscription(\App\AppBundle\Entity\Subscription $subscriptions) {
		$this->subscriptions[] = $subscriptions;

		return $this;
	}

	/**
	 * Remove subscriptions
	 *
	 * @param \App\AppBundle\Entity\Subscription $subscriptions
	 */
	public function removeSubscription(\App\AppBundle\Entity\Subscription $subscriptions) {
		$this->subscriptions->removeElement($subscriptions);
	}

	/**
	 * Get subscriptions
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSubscriptions() {
		return $this->subscriptions;
	}

	/**
	 * Set type
	 *
	 * @param \App\AppBundle\Entity\SlotType $type
	 * @return Slot
	 */
	public function setType(\App\AppBundle\Entity\SlotType $type = null) {
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return \App\AppBundle\Entity\SlotType
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Slot
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Add waitingSubscriptions
	 *
	 * @param \App\AppBundle\Entity\Subscription $waitingSubscriptions
	 * @return Slot
	 */
	public function addWaitingSubscription(\App\AppBundle\Entity\Subscription $waitingSubscriptions) {
		$this->waitingSubscriptions[] = $waitingSubscriptions;

		return $this;
	}

	/**
	 * Remove waitingSubscriptions
	 *
	 * @param \App\AppBundle\Entity\Subscription $waitingSubscriptions
	 */
	public function removeWaitingSubscription(\App\AppBundle\Entity\Subscription $waitingSubscriptions) {
		$this->waitingSubscriptions->removeElement($waitingSubscriptions);
	}

	/**
	 * Get waitingSubscriptions
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getWaitingSubscriptions() {
		return $this->waitingSubscriptions;
	}

	/**
	 * Set isFull
	 *
	 * @param integer $isFull
	 * @return Slot
	 */
	public function setIsFull($isFull)
	{
		$this->isFull = $isFull;

		return $this;
	}

	/**
	 * Get isFull
	 *
	 * @return integer
	 */
	public function getIsFull()
	{
		return $this->isFull;
	}
}
