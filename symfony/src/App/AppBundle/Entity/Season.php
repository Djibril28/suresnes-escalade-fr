<?php

namespace App\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Season
 *
 * @ORM\Table(name="wp_app_season")
 * @ORM\Entity(repositoryClass="App\AppBundle\Repository\SeasonRepository")
 *
 *
 * tarifs:
 *  - licence enfant
 *  - licence adulte
 *  - licence famille
 *
 *  - part CR+CD
 *
 *  - part club enfant
 *  - part club adulte
 *
 *  - surcout non-suresnois enfant
 *  - surcout non-suresnois adulte
 *
 *  - surcout 1er inscription
 *
 *  - option slackline
 *  - option ski
 *
 *  - Sans Assurance
 *  - Assurance Base
 *  - Assurance Base +
 *  - Assurance Base ++
 *
 *  - option IJ 1
 *  - option IJ 2
 *  - option IJ 3
 *
 * calcul = licence
 *     + [part CR+CD]
 *     + [assurance]
 *     + [options IJ]
 *     + [options SLACKLINE]
 *     + [option SKI]
 *     + [part club]
 *     + [surcout non-suresnois]
 *     + [surcout 1er inscription]
 *
 *     => % pro rata inscription en cours d'année
 *
 * inscription
 *     - age
 *     - choix assurance (liste de 4)
 *     - choix option slackline
 *     - choix option ski
 *     - choix IJ
 *
 *     - total calculé
 *     - total dérogatoire
 *     - date de début de validité (pour le tarif)
 *
 *     - checkbox (je veux juste une licence, pas de choix de creneau)
 */
class Season {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var DateTime
	 * @ORM\Column(type="string", length=50)
	 */
	private $title;

	/**
	 * @var DateTime
	 * @ORM\Column(type="datetime")
	 */
	private $beginAt;

	/**
	 * @var DateTime
	 * @ORM\Column(type="datetime")
	 */
	private $endAt;

	/**
	 * @ORM\OneToMany(targetEntity="Slot", mappedBy="season", cascade={"all"}, orphanRemoval=false)
	 * @ORM\OrderBy({"dayOfWeek" = "ASC", "beginTime" = "ASC"})
	 */
	private $slots;

	// Licence

	/**
	 * @var float Prix "Licence Famille"
	 * @ORM\Column(name="price_licence_child", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceLicenceChild;

	/**
	 * @var float Prix "Licence Famille"
	 * @ORM\Column(name="price_licence_adult", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceLicenceAdult;

	/**
	 * @var float Prix "Licence Famille" (à partir de 3 personnes)
	 * @ORM\Column(name="price_licence_family", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceLicenceFamily;

	/**
	 * @var float Prix "Première inscription" (frais de dossier)
	 * @ORM\Column(name="price_first_registration", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceFirstRegistration;

	/**
	 * @var float Prix "Enfant Non-Suresnois"
	 * @ORM\Column(name="price_not_resident_child", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceNotResidentChild;

	/**
	 * @var float Prix "Adulte Non-Suresnois"
	 * @ORM\Column(name="price_not_resident_adult", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceNotResidentAdult;

	// Additions

	/**
	 * @var float Prix "Part CD + CR"
	 * @ORM\Column(name="price_committee", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceCommittee;

	/**
	 * @var float Prix "Part club enfant"
	 * @ORM\Column(name="price_club_child", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceClubChild;

	/**
	 * @var float Prix "Part club adulte"
	 * @ORM\Column(name="price_club_adult", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceClubAdult;

	// Insurance

	/**
	 * @var float Prix "Sans assurance"
	 * @ORM\Column(name="price_insurance0", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceInsurance0;

	/**
	 * @var float Prix "Assurance base"
	 * @ORM\Column(name="price_insurance1", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceInsurance1;

	/**
	 * @var float Prix "Assurance base +"
	 * @ORM\Column(name="price_insurance2", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceInsurance2;

	/**
	 * @var float Prix "Assurance base ++"
	 * @ORM\Column(name="price_insurance3", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceInsurance3;

	/**
	 * @var float Prix "Assurance option Slackline"
	 * @ORM\Column(name="price_insurance_slackline", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceInsuranceSlackline;

	/**
	 * @var float Prix "Assurance option Ski"
	 * @ORM\Column(name="price_insurance_ski", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceInsuranceSki;

	/**
	 * @var float Prix "Assurance option IJ 1"
	 * @ORM\Column(name="price_options_ij1", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceOptionIJ1;

	/**
	 * @var float Prix "Assurance option IJ 2"
	 * @ORM\Column(name="price_options_ij2", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceOptionIJ2;

	/**
	 * @var float Prix "Assurance option IJ 3"
	 * @ORM\Column(name="price_options_ij3", type="float", nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Range(min=0, max=500)
	 * @Assert\Type(type="float")
	 */
	private $priceOptionIJ3;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->slots = new ArrayCollection();
	}

	/**
	 * Compute the subscription price
	 *
	 * @param  Subscription $subscription
	 * @return float
	 *
	 *
	 * calcul = licence
	 *     + [part CR+CD]
	 *     + [assurance]
	 *     + [options IJ]
	 *     + [option SLACKLINE]
	 *     + [option SKI]
	 *     + [part club]
	 *     + [surcout non-suresnois]
	 *     + [surcout 1er inscription]
	 */
	public function getSubscriptionPrice(Subscription $subscription) {
		$isChild = $subscription->getAge() < 18;

		$price = 0;

		if ($subscription->getLicenceFamily()) {
			$price += $this->priceLicenceFamily;
		} elseif ($isChild) {
			$price += $this->priceLicenceChild;
		} else {
			$price += $this->priceLicenceAdult;
		}

		$price += $isChild ? $this->priceClubChild : $this->priceClubAdult;
		$price += $this->priceCommittee;

		switch ($subscription->getInsurance()) {
			case 0:$price += $this->priceInsurance0;break;
			case 1:$price += $this->priceInsurance1;break;
			case 2:$price += $this->priceInsurance2;break;
			case 3:$price += $this->priceInsurance3;break;
		}

		switch ($subscription->getInsuranceIj()) {
			case 1:$price += $this->priceOptionIJ1;break;
			case 2:$price += $this->priceOptionIJ2;break;
			case 3:$price += $this->priceOptionIJ3;break;
		}

		if ($subscription->getInsuranceSlackline()) {
			$price += $this->priceInsuranceSlackline;
		}

		if ($subscription->getInsuranceSki()) {
			$price += $this->priceInsuranceSki;
		}

		if (!$subscription->getIsRenewal()) {
			$price += $this->priceFirstRegistration;
		}

		if ('SURESNES' !== strtoupper(trim($subscription->getCity()))) {
			$price += $isChild ? $this->priceNotResidentChild : $this->priceNotResidentAdult;
		}

		if ($price <= 0) {
			throw new \UnexpectedValueException('The price cannot be Zero');
		}

		return $price;
	}

	/**
	 * @return  string
	 */
	public function __toString() {
		return sprintf('%s', $this->title);
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Season
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set beginAt
	 *
	 * @param \DateTime $beginAt
	 * @return Season
	 */
	public function setBeginAt($beginAt) {
		$this->beginAt = $beginAt;

		return $this;
	}

	/**
	 * Get beginAt
	 *
	 * @return \DateTime
	 */
	public function getBeginAt() {
		return $this->beginAt;
	}

	/**
	 * Set endAt
	 *
	 * @param \DateTime $endAt
	 * @return Season
	 */
	public function setEndAt($endAt) {
		$this->endAt = $endAt;

		return $this;
	}

	/**
	 * Get endAt
	 *
	 * @return \DateTime
	 */
	public function getEndAt() {
		return $this->endAt;
	}

	/**
	 * Add slots
	 *
	 * @param \App\AppBundle\Entity\Slot $slots
	 * @return Season
	 */
	public function addSlot(\App\AppBundle\Entity\Slot $slots) {
		$this->slots[] = $slots;

		return $this;
	}

	/**
	 * Remove slots
	 *
	 * @param \App\AppBundle\Entity\Slot $slots
	 */
	public function removeSlot(\App\AppBundle\Entity\Slot $slots) {
		$this->slots->removeElement($slots);
	}

	/**
	 * Get slots
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSlots() {
		return $this->slots;
	}

	public function getSlotsForSubscription(Subscription $subscription) {
		$slots = array();

		$birthday = $subscription->getProfile()->getBirthday();
		foreach ($this->getSlots() as $slot) {
			if ($slot->getMaxBith() && $slot->getMaxBith() > $birthday) {
				continue;
			}
			if ($slot->getMinBirht() && $slot->getMinBirht() < $birthday) {
				continue;
			}
			$slots[] = $slot;
		}

		return $slots;
	}

	public function getInsuranceChoices() {
		return array(
			1 => sprintf('Assurance base (+ %d €)', $this->priceInsurance1),
			2 => sprintf('Assurance base+ (+ %d €)', $this->priceInsurance2),
			3 => sprintf('Assurance base++ (+ %d €)', $this->priceInsurance3),
			// 0 => 'Refuser les garanties de personnes (pas d\'assurance individuelle accident, rapatriement, frais de recherche secours) : responsabilité civile uniquement (+ 3 €)',
			0 => sprintf('Responsabilité civile uniquement (+ %d €)', $this->priceInsurance0),
		);
	}

	public function getInsuranceChoicesForSubscription() {
		$choices = $this->getInsuranceChoices();

		unset($choices[0]);

		return $choices;
	}

	public function getInsuranceIjChoices() {
		return array(
			0 => 'Pas d\'option IJ',
			1 => sprintf('IJ1 (+ %d €)', $this->priceOptionIJ1),
			2 => sprintf('IJ2 (+ %d €)', $this->priceOptionIJ2),
			3 => sprintf('IJ3 (+ %d €)', $this->priceOptionIJ3),
		);
	}

	public function getInsuranceSlacklineChoices() {
		return array(
			0 => 'Non',
			1 => sprintf('Oui (+ %d €)', $this->priceInsuranceSlackline),
		);
	}

	public function getInsuranceSkiChoices() {
		return array(
			0 => 'Non',
			1 => sprintf('Oui (+ %d €)', $this->priceInsuranceSki),
		);
	}

	/**
	 * Set priceLicenceChild
	 *
	 * @param float $priceLicenceChild
	 * @return Season
	 */
	public function setPriceLicenceChild($priceLicenceChild) {
		$this->priceLicenceChild = $priceLicenceChild;

		return $this;
	}

	/**
	 * Get priceLicenceChild
	 *
	 * @return float
	 */
	public function getPriceLicenceChild() {
		return $this->priceLicenceChild;
	}

	/**
	 * Set priceLicenceAdult
	 *
	 * @param float $priceLicenceAdult
	 * @return Season
	 */
	public function setPriceLicenceAdult($priceLicenceAdult) {
		$this->priceLicenceAdult = $priceLicenceAdult;

		return $this;
	}

	/**
	 * Get priceLicenceAdult
	 *
	 * @return float
	 */
	public function getPriceLicenceAdult() {
		return $this->priceLicenceAdult;
	}

	/**
	 * Set priceLicenceFamily
	 *
	 * @param float $priceLicenceFamily
	 * @return Season
	 */
	public function setPriceLicenceFamily($priceLicenceFamily) {
		$this->priceLicenceFamily = $priceLicenceFamily;

		return $this;
	}

	/**
	 * Get priceLicenceFamily
	 *
	 * @return float
	 */
	public function getPriceLicenceFamily() {
		return $this->priceLicenceFamily;
	}

	/**
	 * Set priceFirstRegistration
	 *
	 * @param float $priceFirstRegistration
	 * @return Season
	 */
	public function setPriceFirstRegistration($priceFirstRegistration) {
		$this->priceFirstRegistration = $priceFirstRegistration;

		return $this;
	}

	/**
	 * Get priceFirstRegistration
	 *
	 * @return float
	 */
	public function getPriceFirstRegistration() {
		return $this->priceFirstRegistration;
	}

	/**
	 * Set priceNotResidentChild
	 *
	 * @param float $priceNotResidentChild
	 * @return Season
	 */
	public function setPriceNotResidentChild($priceNotResidentChild) {
		$this->priceNotResidentChild = $priceNotResidentChild;

		return $this;
	}

	/**
	 * Get priceNotResidentChild
	 *
	 * @return float
	 */
	public function getPriceNotResidentChild() {
		return $this->priceNotResidentChild;
	}

	/**
	 * Set priceNotResidentAdult
	 *
	 * @param float $priceNotResidentAdult
	 * @return Season
	 */
	public function setPriceNotResidentAdult($priceNotResidentAdult) {
		$this->priceNotResidentAdult = $priceNotResidentAdult;

		return $this;
	}

	/**
	 * Get priceNotResidentAdult
	 *
	 * @return float
	 */
	public function getPriceNotResidentAdult() {
		return $this->priceNotResidentAdult;
	}

	/**
	 * Set priceCommittee
	 *
	 * @param float $priceCommittee
	 * @return Season
	 */
	public function setPriceCommittee($priceCommittee) {
		$this->priceCommittee = $priceCommittee;

		return $this;
	}

	/**
	 * Get priceCommittee
	 *
	 * @return float
	 */
	public function getPriceCommittee() {
		return $this->priceCommittee;
	}

	/**
	 * Set priceClubChild
	 *
	 * @param float $priceClubChild
	 * @return Season
	 */
	public function setPriceClubChild($priceClubChild) {
		$this->priceClubChild = $priceClubChild;

		return $this;
	}

	/**
	 * Get priceClubChild
	 *
	 * @return float
	 */
	public function getPriceClubChild() {
		return $this->priceClubChild;
	}

	/**
	 * Set priceClubAdult
	 *
	 * @param float $priceClubAdult
	 * @return Season
	 */
	public function setPriceClubAdult($priceClubAdult) {
		$this->priceClubAdult = $priceClubAdult;

		return $this;
	}

	/**
	 * Get priceClubAdult
	 *
	 * @return float
	 */
	public function getPriceClubAdult() {
		return $this->priceClubAdult;
	}

	/**
	 * Set priceInsurance0
	 *
	 * @param float $priceInsurance0
	 * @return Season
	 */
	public function setPriceInsurance0($priceInsurance0) {
		$this->priceInsurance0 = $priceInsurance0;

		return $this;
	}

	/**
	 * Get priceInsurance0
	 *
	 * @return float
	 */
	public function getPriceInsurance0() {
		return $this->priceInsurance0;
	}

	/**
	 * Set priceInsurance1
	 *
	 * @param float $priceInsurance1
	 * @return Season
	 */
	public function setPriceInsurance1($priceInsurance1) {
		$this->priceInsurance1 = $priceInsurance1;

		return $this;
	}

	/**
	 * Get priceInsurance1
	 *
	 * @return float
	 */
	public function getPriceInsurance1() {
		return $this->priceInsurance1;
	}

	/**
	 * Set priceInsurance2
	 *
	 * @param float $priceInsurance2
	 * @return Season
	 */
	public function setPriceInsurance2($priceInsurance2) {
		$this->priceInsurance2 = $priceInsurance2;

		return $this;
	}

	/**
	 * Get priceInsurance2
	 *
	 * @return float
	 */
	public function getPriceInsurance2() {
		return $this->priceInsurance2;
	}

	/**
	 * Set priceInsurance3
	 *
	 * @param float $priceInsurance3
	 * @return Season
	 */
	public function setPriceInsurance3($priceInsurance3) {
		$this->priceInsurance3 = $priceInsurance3;

		return $this;
	}

	/**
	 * Get priceInsurance3
	 *
	 * @return float
	 */
	public function getPriceInsurance3() {
		return $this->priceInsurance3;
	}

	/**
	 * Set priceInsuranceSlackline
	 *
	 * @param float $priceInsuranceSlackline
	 * @return Season
	 */
	public function setPriceInsuranceSlackline($priceInsuranceSlackline) {
		$this->priceInsuranceSlackline = $priceInsuranceSlackline;

		return $this;
	}

	/**
	 * Get priceInsuranceSlackline
	 *
	 * @return float
	 */
	public function getPriceInsuranceSlackline() {
		return $this->priceInsuranceSlackline;
	}

	/**
	 * Set priceInsuranceSki
	 *
	 * @param float $priceInsuranceSki
	 * @return Season
	 */
	public function setPriceInsuranceSki($priceInsuranceSki) {
		$this->priceInsuranceSki = $priceInsuranceSki;

		return $this;
	}

	/**
	 * Get priceInsuranceSki
	 *
	 * @return float
	 */
	public function getPriceInsuranceSki() {
		return $this->priceInsuranceSki;
	}

	/**
	 * Set priceOptionIJ1
	 *
	 * @param float $priceOptionIJ1
	 * @return Season
	 */
	public function setPriceOptionIJ1($priceOptionIJ1) {
		$this->priceOptionIJ1 = $priceOptionIJ1;

		return $this;
	}

	/**
	 * Get priceOptionIJ1
	 *
	 * @return float
	 */
	public function getPriceOptionIJ1() {
		return $this->priceOptionIJ1;
	}

	/**
	 * Set priceOptionIJ2
	 *
	 * @param float $priceOptionIJ2
	 * @return Season
	 */
	public function setPriceOptionIJ2($priceOptionIJ2) {
		$this->priceOptionIJ2 = $priceOptionIJ2;

		return $this;
	}

	/**
	 * Get priceOptionIJ2
	 *
	 * @return float
	 */
	public function getPriceOptionIJ2() {
		return $this->priceOptionIJ2;
	}

	/**
	 * Set priceOptionIJ3
	 *
	 * @param float $priceOptionIJ3
	 * @return Season
	 */
	public function setPriceOptionIJ3($priceOptionIJ3) {
		$this->priceOptionIJ3 = $priceOptionIJ3;

		return $this;
	}

	/**
	 * Get priceOptionIJ3
	 *
	 * @return float
	 */
	public function getPriceOptionIJ3() {
		return $this->priceOptionIJ3;
	}
}
