<?php

namespace App\AppBundle\Service;

use App\AppBundle\Entity\Season;
use App\AppBundle\Entity\Subscription;
use Doctrine\ORM\EntityManagerInterface;
use Hype\MailchimpBundle\Mailchimp\MailChimp as HypeMailChimp;

class MailChimp
{
    private $em;

    private $mailchimp;

    public function __construct(EntityManagerInterface $em, HypeMailChimp $mc)
    {
        $this->em = $em;
        $this->mailchimp = $mc;
    }

    public function syncList(Season $season)
    {
        $subscriptions = $this->em->getRepository('AppBundle:Subscription')
            ->createQueryBuilder('s')
            ->select('s')
            ->where('s.season = :season')
            ->andWhere('s.status = :status')
            ->getQuery()
            ->setParameter('season', $season)
            ->setParameter('status', Subscription::STATUS_ACTIVE)
            ->execute();

        $competitorChoices = Subscription::getIsCompetitorChoices();

        foreach ($subscriptions as $subscription) {
            $data[] = $this->getSubscriptionData($subscription, $season);
        }

        if (empty($data)) {
            throw new \RuntimeException('Empty list !');
        }

        $response = $this->mailchimp->getList()->batchSubscribe($data, false, true, true);

        return $response;
    }

    public function syncSubscription(Subscription $subscription)
    {
        $data = array();
        $data[] = $this->getSubscriptionData($subscription, $subscription->getSeason());

        $response = $this->mailchimp->getList()->batchSubscribe($data, false, true, true);

        return $response;
    }

    private function getSubscriptionData(Subscription $subscription, Season $season)
    {
        return array(
            'email' => array('email' => $subscription->getEmail()),
            'email_type' => 'html',
            'merge_vars' => array(
                'PRENOM'    => $subscription->getFirstName(),
                'NOM'       => $subscription->getLastName(),
                'AGE'       => $subscription->getAge(),
                'SLOT'      => $subscription->getActivatedSlot() ? $subscription->getActivatedSlot()->getTitle() : 'Aucun',
                'LICENCE'   => $subscription->getLicence(),
                'COMPET'    => $subscription->getIsCompetitor() ? $competitorChoices[$subscription->getIsCompetitor()] : 'Inconnu',
                'SAISON'    => $subscription->isActive() ? $season->getBeginAt()->format('Y') : null,
            ),
        );
    }
}
