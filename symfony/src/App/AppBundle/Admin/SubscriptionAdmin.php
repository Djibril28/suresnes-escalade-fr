<?php

namespace App\AppBundle\Admin;

use App\AppBundle\Entity\Subscription;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class SubscriptionAdmin extends Admin
{
    private $urlGenerator;

    public function setUrlGenerator(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('print', $this->getRouterIdParameter().'/print');
        $collection->add('voucher', $this->getRouterIdParameter().'/voucher');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('season')
            ->add('lastName')
            ->add('firstName')
            ->add('status', null, array('choices' => Subscription::getStatusChoices()))
            ->add('activatedBy')
            ->add('licence')
            ->add('slot1')
            ->add('slot2')
            ->add('activatedSlot')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('season')
            ->add('lastName')
            ->add('firstName')
            ->add('status', 'choice', array('choices' => Subscription::getStatusChoices()))
            ->add('createdAt')
            ->add('age')
            ->add('priceCalculated')
            ->add('activatedSlot')
            ->add('licence')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'print' => array(
                        'template' => 'AppBundle:Admin:list__action_print.html.twig'
                    ),
                    'voucher' => array(
                        'template' => 'AppBundle:Admin:list__action_voucher.html.twig'
                    ),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subscription = $this->getSubject();
        $season = $subscription->getSeason();

        // Link to certificate file
        if ($subscription->getCertificateFile()) {
            $certificatFileHelp = sprintf('<a href="%s" title="Télécharger" target="_blank">Afficher le fichier "%s"</a>',
                $this->urlGenerator->generate('simple_subscription_certificat', array('id' => $subscription->getId())),
                $subscription->getCertificateFile());
        } else {
            $certificatFileHelp = 'Aucun fichier';
        }

        // Available slots
        $slotOptions = array(
            'choices'     => $season->getSlots(),
            'property'    => 'label',
            'required'    => false,
            'empty_value' => '—',
        );

        // Form
        $formMapper
            ->with('Profil du grimpeur')
            // ->add('userId', 'entity', array('class' => 'EkinoWordpressBundle:User', 'property' => 'displayName'))
                ->add('season', null, array('disabled' => true))
                ->add('lastName', null, array('required' => true))
                ->add('firstName', null, array('required' => true))
                ->add('birthday', 'birthday')
                ->add('age', null, array('disabled' => true))
                ->add('sex', 'choice', array('choices' => array('M' => 'Masculin', 'F' => 'Féminin'), 'expanded' => true))
                ->add('status', 'choice', array('required' => true, 'choices' => Subscription::getStatusChoices()))
                ->add('activatedAt', null, array('disabled' => true))
                ->add('activatedBy', null, array('disabled' => true))
                ->add('comment', 'textarea', array('required' => false))
                ->add('address')
                ->add('postalCode', 'text')
                ->add('city')
                ->add('phone', 'text')
                ->add('email', 'email')
                ->add('occupation')
                ->add('nationality', 'choice', array('required' => true, 'choices' => Subscription::getNationalityChoices()))
            ->end()
            ->with('Adhérent parent')
                ->add('parentLastName', null, array('required' => false))
                ->add('parentFirstName', null, array('required' => false))
                ->add('parentPhone', 'text', array('required' => false))
                ->add('parentEmail', 'email', array('required' => false))
            ->end()
            ->with('Créneau')
                ->add('slot1', null, $slotOptions)
                ->add('slot2', null, $slotOptions)
                ->add('activatedSlot', null, $slotOptions)
                ->add('noSlot', 'checkbox', array('required' => false))
            ->end()
            ->with('Informations')
                ->add('isRenewal', 'checkbox', array('required' => false))
                ->add('climbingLevel', 'choice', array('choices' => Subscription::getClimbingLevels()))
                ->add('climbingExperience', 'choice', array('choices' => Subscription::getClimbingExperiences()))
                ->add('passportFfme', 'choice', array('choices' => Subscription::getPassportsFFME()))
                ->add('isCompetitor', 'choice', array('choices' => Subscription::getIsCompetitorChoices()))
            ->end()
            ->with('Licence & Assurance')
                ->add('licence', 'text', array('required' => false))
                ->add('licenceDate', 'date', array('required' => false))
                ->add('licenceFamily', 'checkbox', array('required' => false))
                ->add('insurance', 'choice', array('choices' => $season->getInsuranceChoices(), 'expanded' => true))
                ->add('insuranceIj', 'choice', array('choices' => $season->getInsuranceIjChoices(), 'expanded' => true))
                ->add('insuranceSlackline', 'choice', array('choices' => $season->getInsuranceSlacklineChoices(), 'expanded' => true))
                ->add('insuranceSki', 'choice', array('choices' => $season->getInsuranceSkiChoices(), 'expanded' => true))
            ->end()
            ->with('Certificat médical')
                ->add('certificateFile', 'file', array('required' => false, 'data_class' => null, 'help' => $certificatFileHelp))
                ->add('certificateDoctor', null, array('required' => false))
                ->add('certificateDate', 'date', array('required' => false))
                ->add('certificateCompetition', 'checkbox', array('required' => false))
                ->add('certificateStatus', 'choice', array('choices' => Subscription::getCertificateStatusChoices()))
            ->end()
            ->with('Comptabilité')
                // Payment
                ->add('payment3Times', 'checkbox', array('required' => false))
                ->add('paymentPass92', 'checkbox', array('required' => false))
                ->add('paymentCouponSport', 'checkbox', array('required' => false))
                ->add('paymentSubvensionCe', 'checkbox', array('required' => false))

                ->add('priceCalculated', null, array('required' => false, 'disabled' => true, 'help' => 'Recalculé à chaque enregistrement'))
                ->add('priceCorrected', null, array('required' => false))
                ->add('paidCash', null, array('required' => false))
                ->add('paidCB', null, array('required' => false))
                ->add('paidPass92', null, array('required' => false))
                ->add('paidCouponSport', null, array('required' => false))
                ->add('paidSubvensionCe', null, array('required' => false))
                ->add('paidRegul', null, array('required' => false, 'help' => 'Si cet adhérent a payé pour un autre adhérent, mettre le montant de la cotisation de l\'autre adhérent (sans signe négatif). Il faut également penser à mettre le montant négatif de la cotisation sur la fiche de l\'autre adhérent.'))
                ->add('paidCheck01', null, array('required' => false))
                ->add('paidCheck02', null, array('required' => false))
                ->add('paidCheck03', null, array('required' => false))
                ->add('paidCheckGarantee', null, array('required' => false))
                ->add('paidTotal', null, array('required' => false, 'disabled' => true))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('userId')
            ->add('season')
            ->add('lastName')
            ->add('firstName')
            ->add('sex')
            ->add('status')
            ->add('createdAt')
            ->add('activatedAt')
            ->add('activatedBy')
            ->add('birthday', 'date')
            ->add('age')
            ->add('occupation')
            ->add('licence')
            ->add('licenceDate', 'date')
            ->add('slot1')
            ->add('slot2')
            ->add('activatedSlot')
            ->add('certificateFile')
            ->add('certificateDoctor')
            ->add('certificateDate')
        ;
    }

    public function getExportFormats()
    {
        return array(
            'csv', 'xls'
        );
    }

    public function getExportFields()
    {
        $fields = array(
            'season.title' => 'Saison',
            'lastName' => 'Nom',
            'firstName' => 'Prénom',
            'birthday' => 'Date de naissance',
            'age' => 'Age',
            'sex' => 'Sexe',
            'status' => 'Status de l\'inscription',
            'activatedBy' => 'Activé par',
            'comment' => 'Commentaire',
            'address' => 'Adresse',
            'postalCode' => 'Code postal',
            'city' => 'Ville',
            'phone' => 'Téléphone',
            'email' => 'Email',
            'occupation' => 'Profession',
            'nationality' => 'Nationalité',
            'parentLastName' => 'Nom du parent',
            'parentFirstName' => 'Prénom du parent',
            'parentPhone' => 'Téléphone du parent',
            'parentEmail' => 'Email du parent',
            'slot1.title' => 'Créneau 1',
            'slot2.title' => 'Créneau 2',
            'activatedSlot.title' => 'Créneau validé',
            'noSlot' => 'Aucun créneau',
            'isRenewal' => 'Renouvellement',
            'isCompetitor' => 'Compétiteur',
            'licence' => 'Numéro de licence',
            'licenceDate' => 'Date de licence',
            // 'insurance' => 'insurance',
            // 'insuranceIj' => 'insuranceIj',
            // 'insuranceSki' => 'insuranceSki',
            // 'certificateFile' => 'certificateFile',
            // 'certificateDoctor' => 'certificateDoctor',
            // 'certificateDate' => 'certificateDate',
            // 'certificateCompetition' => 'certificateCompetition',
            // 'certificateStatus' => 'certificateStatus',
            'payment3Times' => 'Paiement en 3 fois',
            'paymentPass92' => 'Pass 92',
            'paymentCouponSport' => 'Coupon Sport',
            'paymentSubvensionCe' => 'Subvension CE',
            'priceCalculated' => 'Prix calculé',
            'priceCorrected' => 'Prix corrigé',
            'paidCash' => 'Payé cash',
            'paidCB' => 'Payé CB',
            'paidPass92' => 'Payé Pass 92',
            'paidCouponSport' => 'Payé Coupon Sport',
            'paidSubvensionCe' => 'Payé Subvension CE',
            'paidRegul' => 'Payé par régulation',
            'paidCheck01' => 'Payé chèque 1',
            'paidCheck02' => 'Payé chèque 2',
            'paidCheck03' => 'Payé chèque 3',
            'paidCheckGarantee' => 'Payé chèque garantie',
            'paidTotal' => 'Payé total',
        );

        return array_flip($fields);
    }
}
