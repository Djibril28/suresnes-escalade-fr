<?php

namespace App\AppBundle\Admin;

use App\AppBundle\Entity\Slot;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SlotAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('season')
            ->add('title')
            ->add('isFull')
            // ->add('dayOfWeek', 'doctrine_orm_choice', array('choices' => Slot::getDayChoices()))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('season')
            ->add('title')
            ->add('description')
            ->add('dayOfWeek')
            ->add('beginTime')
            ->add('endTime')
            ->add('maxPlaces')
            ->add('isFull')
            // ->add('minBirth')
            // ->add('maxBirth')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('season')
            ->add('title')
            ->add('description', 'textarea', array('required' => false))
            ->add('dayOfWeek', 'choice', array('choices' => Slot::getDayChoices()))
            ->add('beginTime', 'time')
            ->add('endTime', 'time')
            ->add('maxPlaces', 'integer')
            ->add('marginPlaces', 'integer')
            ->add('isFull', null, array('required' => false))
            // ->add('minBirth', 'birthday')
            // ->add('maxBirth', 'birthday')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('description')
            ->add('dayOfWeek')
            ->add('beginTime')
            ->add('endTime')
            ->add('minBirth')
            ->add('maxBirth')
            ->add('isFull')
        ;
    }
}
