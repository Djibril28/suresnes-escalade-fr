<?php

namespace App\AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SeasonAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        //    ->add('title')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('beginAt')
            ->add('endAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    // 'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'clone' => array(
                        'template' => 'AppBundle:SeasonAdmin:list__action_clone.html.twig',
                    ),
                    'exportFFME' => array(
                        'template' => 'AppBundle:SeasonAdmin:list__action_exportFFME.html.twig',
                    ),
                    'importFFME' => array(
                        'template' => 'AppBundle:SeasonAdmin:list__action_importFFME.html.twig',
                    ),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('title')
                ->add('beginAt', 'date')
                ->add('endAt', 'date')
            ->end()
            ->with('Tarif cotisation')
                ->add('priceFirstRegistration')
                ->add('priceNotResidentChild')
                ->add('priceNotResidentAdult')
                ->add('priceCommittee')
                ->add('priceClubChild')
                ->add('priceClubAdult')
            ->end()
            ->with('Tarif Licence')
                ->add('priceLicenceChild')
                ->add('priceLicenceAdult')
                ->add('priceLicenceFamily')
            ->end()
            ->with('Tarif assurance')
                ->add('priceInsurance0')
                ->add('priceInsurance1')
                ->add('priceInsurance2')
                ->add('priceInsurance3')
                ->add('priceInsuranceSlackline')
                ->add('priceInsuranceSki')
                ->add('priceOptionIJ1')
                ->add('priceOptionIJ2')
                ->add('priceOptionIJ3')
            ->end()
            ->with('Créneaux')
                ->add('slots', 'sonata_type_collection', array(
                    'by_reference' => false,
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable'  => 'position',
                ))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('beginAt')
            ->add('endAt')
            ->add('slots')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clone', $this->getRouterIdParameter().'/clone');
        $collection->add('exportFFME', $this->getRouterIdParameter().'/exportFFME');
        $collection->add('importFFME', $this->getRouterIdParameter().'/importFFME');
    }
}
