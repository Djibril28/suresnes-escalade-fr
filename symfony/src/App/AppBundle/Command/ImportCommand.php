<?php

namespace App\AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use App\AppBundle\Entity;
use App\AppBundle\Entity\Subscription;
use App\AppBundle\Entity\Season;

class ImportCommand extends ContainerAwareCommand
{
    protected $seasons = array();
    protected $slots = array();

    protected $pdo;
    protected $em;

    protected function configure()
    {
        $this
            ->setName('app:import')
            ->setDescription('Import data from the existing site')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->pdo = $this->getContainer()->get('doctrine')->getConnection();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();

        $output->writeln('Import Seasons');
        $this->importSeasons($output);
        $output->writeln('Seasons: '.implode(', ', array_keys($this->seasons)));
        $this->em->flush();

        $output->writeln('Import Slots');
        $this->importSlots($output);
        $this->em->flush();

        $output->writeln('Import Subscriptions');
        $this->importSubscriptions($output);
        $this->em->flush();

        $output->writeln('Saving');
        $this->em->flush();
    }

    protected function importSeasons(OutputInterface $output)
    {
        $stmt = $this->pdo->query('SELECT * FROM se_prix_cotisation ORDER BY annee asc');

        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($results as $row) {
            $season = new Entity\Season();
            $season->setTitle(sprintf('Saison %d-%d', $row['annee'], $row['annee']+1));
            $season->setBeginAt((new \DateTime)->setTimestamp(mktime(0, 0, 0, 6, 1, $row['annee'])));
            $season->setEndAt((new \DateTime)->setTimestamp(mktime(0, 0, 0, 9, 1, $row['annee']+1)));

            $season->setPriceLicenceChild(intval($row['licence_ffme_enfant']));
            $season->setPriceLicenceAdult(intval($row['licence_ffme_adulte']));
            $season->setPriceLicenceFamily(0);
            $season->setPriceFirstRegistration(intval($row['adulte_suresnois_avec_droit_entree']-$row['adulte_suresnois']));
            $season->setPriceNotResidentChild(intval($row['enfant_non_suresnois']-$row['enfant_suresnois']));
            $season->setPriceNotResidentAdult(intval($row['adulte_non_suresnois']-$row['adulte_suresnois']));
            $season->setPriceCommittee(0);
            $season->setPriceClubChild($row['enfant_suresnois']);
            $season->setPriceClubAdult($row['adulte_suresnois']);
            $season->setPriceInsurance0(0);
            $season->setPriceInsurance1(intval($row['assurance_base']));
            $season->setPriceInsurance2(intval($row['assurance_base_plus']));
            $season->setPriceInsurance3(0);
            $season->setPriceInsuranceSki(intval($row['assurance_option_ski']));
            $season->setPriceOptionIJ1(0);
            $season->setPriceOptionIJ2(0);
            $season->setPriceOptionIJ3(0);

            $this->em->persist($season);

            $this->seasons[$row['annee']] = $season;
        }

        $season = new Entity\Season();
        $season->setTitle('Saison 2014-2015');
        $season->setBeginAt((new \DateTime)->setTimestamp(mktime(0, 0, 0, 6, 1, 2014)));
        $season->setEndAt((new \DateTime)->setTimestamp(mktime(0, 0, 0, 9, 1, 2015)));

        $season->setPriceLicenceChild(40.45);
        $season->setPriceLicenceAdult(53.15);
        $season->setPriceLicenceFamily(11.50); // ?
        $season->setPriceFirstRegistration(30);
        $season->setPriceNotResidentChild(20);
        $season->setPriceNotResidentAdult(20);
        $season->setPriceCommittee(0);
        $season->setPriceClubChild(225 - $season->getPriceLicenceChild() - $season->getPriceCommittee());
        $season->setPriceClubAdult(240 - $season->getPriceLicenceAdult() - $season->getPriceCommittee());
        $season->setPriceInsurance0(3);
        $season->setPriceInsurance1(10);
        $season->setPriceInsurance2(13);
        $season->setPriceInsurance3(20);
        $season->setPriceInsuranceSki(5);
        $season->setPriceOptionIJ1(18);
        $season->setPriceOptionIJ2(30);
        $season->setPriceOptionIJ3(35);

        $this->em->persist($season);
        $this->seasons['2014'] = $season;
    }

    protected function importSlots(OutputInterface $output)
    {
        $days = array(
            'lundi'     => 1,
            'mardi'     => 2,
            'mercredi'  => 3,
            'jeudi'     => 4,
            'vendredi'  => 5,
            'samedi'    => 6,
            'dimanche'  => 7,
        );

        $stmt = $this->pdo->query('SELECT * FROM se_creneaux ORDER BY creneaux_annee, creneaux_ordre, creneaux_grimpeurs_max');

        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($results as $row) {
            $slot = new Entity\Slot();

            $slot->setTitle($row['creneaux_affichage']);
            $slot->setSeason($this->seasons[$row['creneaux_annee']]);
            $slot->setDayOfWeek($days[$row['creneaux_jour']]);
            $slot->setBeginTime((new \DateTime)->setTimestamp(strtotime(str_replace('h', ':', $row['creneaux_heure_debut']))));
            $slot->setEndTime((new \DateTime)->setTimestamp(strtotime(str_replace('h', ':', $row['creneaux_heure_fin']))));
            $slot->setMaxPlaces(intval($row['creneaux_grimpeurs_max']));

            $this->em->persist($slot);

            $this->slots[$row['creneaux_id']] = $slot;

            if (2013 == $row['creneaux_annee']) {
                $slot = clone $slot;
                $slot->setSeason($this->seasons[2014]);
                $this->em->persist($slot);
            }
        }
    }

    protected function importSubscriptions(OutputInterface $output)
    {
        $stmt = $this->pdo->query('SELECT * FROM se_adherent WHERE annee <> 0 ORDER BY annee, date_inscription');

        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $users = $this->getContainer()->get('ekino.wordpress.manager.user');

        foreach ($results as $row) {
            // $user = $users->findOneBy(array('email' => $row['email_adherent']));

            $subscription = new Entity\Subscription();
            // $subscription->setUserId($user ? $user->getId() : null);
            $subscription->setSeason($this->seasons[$row['annee']]);
            $subscription->setLastName($row['nom_grimpeur'] ?: $row['nom_adherent']);
            $subscription->setFirstName($row['prenom_grimpeur'] ?: $row['prenom_adherent']);
            $subscription->setEmail($row['email_grimpeur']);
            $subscription->setPhone($row['tel_port_grimpeur'] ?: $row['tel_fixe_grimpeur']);
            $subscription->setParentLastName($row['nom_adherent']);
            $subscription->setParentFirstName($row['prenom_adherent']);
            $subscription->setParentEmail($row['email_adherent']);
            $subscription->setParentPhone($row['tel_port_adherent'] ?: $row['tel_fixe_adherent']);
            $subscription->setSex($row['sexe'] ? $row['sexe'][0] : null);
            $subscription->setStatus('actif' == $row['actif'] ? Subscription::STATUS_ACTIVE : Subscription::STATUS_WAITING);
            $subscription->setIsRenewal('non' == $row['nouvel_adherent']);
            $subscription->setOccupation($row['profession_grimpeur'] ?: $row['profession_adherent']);
            $subscription->setCreatedAt((new \DateTime())->setTimestamp(mktime(0, 0, 0, $row['date_inscription_mois'], $row['date_inscription_jour'], $row['date_inscription_annee'])));
            $subscription->setAge($row['age']);
            $subscription->setBirthday((new \DateTime())->setTimestamp(mktime(0, 0, 0, $row['date_naissance_mois'], $row['date_naissance_jour'], $row['date_naissance_annee'])));
            $subscription->setIsCompetitor(in_array($row['participation_compet'], array(1, 'oui', 'pourquoipas')));

            $subscription->setCertificateDoctor($row['certificat_medical_medecin']);
            if (!empty($row['certificat_medical'])) {
                $subscription->setCertificateCompetition('loisir' != $row['certificat_medical']);
            }

            if ($row['creneau_id_souhait1'] && array_key_exists($row['creneau_id_souhait1'], $this->slots)) {
                $subscription->setSlot1($this->slots[$row['creneau_id_souhait1']]);
            }
            if ($row['creneau_id_souhait2'] && array_key_exists($row['creneau_id_souhait2'], $this->slots)) {
                $subscription->setSlot2($this->slots[$row['creneau_id_souhait2']]);
            }
            if ($subscription->isActive()) {
                $subscription->setActivatedAt((new \DateTime)->setTimestamp(mktime(0, 0, 0, $row['date_activation_mois'], $row['date_activation_jour'], $row['date_activation_annee'])));

                $subscription->setActivatedBy($row['activateur']);
            }
            if ($row['creneau'] && array_key_exists($row['creneau'], $this->slots)) {
                $subscription->setActivatedSlot($this->slots[$row['creneau']]);
            }

            $subscription->setIsRenewal('oui' != $row['nouvel_adherent']);

            $subscription->setInsurance(strpos('base_plus', $row['assurance_garantie_auto'])?2:1);
            $insuranceIj = array('aucune' => 0, 'option_a' => 1, 'option_b' => 2, 'option_c' => 3, 'autre' => 0);
            $subscription->setInsuranceIj(@$insuranceIj[$row['assurance_garantie_auto']]?:0);
            $subscription->setInsuranceSki((bool) strpos('ski', $row['assurance_garantie_auto']));
            $subscription->setPayment3Times(0);

            $subscription->setPriceCalculated($row['prix_cotisation']);
            $subscription->setPriceCorrected($row['prix_cotisation_modif_admin']);

            $subscription->setPaidPass92($row['paiement_pass92']);
            $subscription->setPaidCouponSport($row['paiement_coupons_sport']);
            $subscription->setPaidCheck01($row['paiement_cheque']);
            $subscription->setPaidCheck02($row['paiement_cheque_01']+$row['paiement_cheque_02']+$row['paiement_cheque_03']+$row['paiement_cheque_04']);
            $subscription->setPaidCheck03($row['paiement_cheque_05']+$row['paiement_cheque_06']+$row['paiement_cheque_07']+$row['paiement_cheque_08']+$row['paiement_cheque_09']+$row['paiement_cheque_10']+$row['paiement_cheque_11']+$row['paiement_cheque_12']);
            $subscription->setPaidRegul($row['regularisation']);
            $subscription->setPaidCash($row['paiement_especes']);

            $this->em->persist($subscription);
        }
    }
}
