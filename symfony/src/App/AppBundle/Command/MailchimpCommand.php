<?php

namespace App\AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use App\AppBundle\Entity;
use App\AppBundle\Entity\Subscription;
use App\AppBundle\Entity\Season;

class MailchimpCommand extends ContainerAwareCommand
{
    protected $seasons = array();
    protected $slots = array();

    protected $em;

    protected function configure()
    {
        $this
            ->setName('app:mailchimp')
            ->setDescription('Export to Mailchimp')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $season = $this->em->getRepository('AppBundle:Season')->findCurrent();

        $response = $this->getContainer()->get('mailchimp')->syncList($season);

        $output->writeln(sprintf('<info>%d</info> contacts ajoutés', $response['add_count']));
        $output->writeln(sprintf('<info>%d</info> contacts mis à jour', $response['update_count']));
        if ($response['error_count'] > 0) {
            $output->writeln(sprintf('<error>%d errors</error>', $response['error_count']));
        } else {
            $output->writeln(sprintf('<info>%d</info> contacts en erreur', $response['error_count']));
        }
    }
}
