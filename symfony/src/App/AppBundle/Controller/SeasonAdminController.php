<?php

namespace App\AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use App\AppBundle\Entity\Subscription;
use Exporter\Writer\CsvWriter;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SeasonAdminController extends CRUDController
{
    public function exportFFMEAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $subscriptions = $this->getDoctrine()->getRepository('AppBundle:Subscription')->findBy(array(
            'season' => $object,
            'status' => Subscription::STATUS_ACTIVE,
            'licenceDate' => null,
        ), array(
            'activatedAt' => 'ASC',
            'createdAt' => 'ASC',
        ));

        $writer = new CsvWriter('php://output', ';', '"', "", true, true);

        $callback = function() use ($subscriptions, $writer) {
            $writer->open();

            $e = function ($string) {
                return twig_convert_encoding($string, 'CP1252', 'UTF-8');
            };
            $f = function ($phone) {
                return preg_replace('/[^0-9]/', '', $phone);
            };

            $insurances = array(0 => 'RC', 1 => 'B', 2 => 'B+', 3 => 'B++');
            $insurancesIj = array(0 => 'NON', 1 => 'IJ1', 2 => 'IJ2', 3 => 'IJ3');
            foreach ($subscriptions as $s) {
                $writer->write(array(
                    'ACTION' => $s->getLicence() ? 'R' : 'C', // Action  C, R ou MAJ
                    'NOM' => $e(substr($s->getLastName(), 0, 100)), // Nom 100 caractères maximum
                    'PRENOM' => $e(substr($s->getFirstName(), 0, 100)), // Prénom  100 caractères maximum
                    'DATE DE NAISSANCE' => $s->getBirthday()->format('d/m/Y'), // Date naissance  format : jj/mm/aaaa
                    'SEXE' => $s->getSex() == 'F' ? 'F' : 'H', // Sexe    H ou F
                    'NATIONALITE' => $s->getNationality() ?: 'FR', // Nationalité 2 caractères (voir tableau des codes pays)
                    'ADRESSE' => $e(substr($s->getAddress(), 0, 255)), // Adresse 255 caractères maximum
                    'ADRESSE COMPLEMENT' => $e(substr($s->getAddress(), 255, 255*2)), // Complément d'adresse    255 caractères maximum
                    'CODE POSTAL' => $s->getPostalCode(), // Code postal Si Pays=France, code postal de 5 chiffres
                    'VILLE' => $s->getCity(), // Ville   100 caractères maximum
                    'PAYS' => 'FR', // Pays    2 caractères (voir tableau des codes pays)
                    'TEL FIXE' => $f($s->getPhone()), // Téléphone   Si Pays=France, 10 chiffres
                    'TEL MOBILE' => $f($s->getPhone()), // Mobile  Si Pays=France, 10 chiffres  (au moins 1 des 2)
                    'COURRIEL' => $s->getEmail(), // Courriel    100 caractères maximum
                    'COURRIEL 2' => $s->getParentEmail(),  // Courriel  Parent
                    'GROUPE' => $s->getActivatedSlot() ? $s->getActivatedSlot()->getLabel() : '', // Champ libre pour affecter un licencié à un groupe
                    'PAP NOM' => $e(substr($s->getParentLastName(), 0, 100)), // Personne à prévenir - Nom   100 caractères maximum
                    'PAP PRENOM' => $e(substr($s->getParentFirstName(), 0, 100)), // Personne à prévenir - Prénom    100 caractères maximum
                    'PAP ADRESSE' => '', // Personne à prévenir - Adresse   255 caractères maximum
                    'PAP ADRESSE COMPLEMENT' => '', // Personne à prévenir - Compl. d'adresse  255 caractères maximum
                    'PAP CODE POSTAL' => '', // Personne à prévenir - Code postal   10 caractères maximum
                    'PAP VILE' => '', // TYPO ?? Personne à prévenir - Ville 100 caractères maximum
                    'PAP TELEPHONE' => $f($s->getParentPhone()), // Personne à prévenir - Téléphone 15 caractères maximum
                    'PAP COURRIEL' => $s->getParentEmail(), // Personne à prévenir - Courriel  100 caractères maximum
                    'NUMERO DE LICENCE' => $s->getLicence(), // N° licence  6 chiffres  AUTOMATIQUE
                    'TYPE LICENCE' => $s->getAge() < 18 ? 'J' : 'A', // Type licence    J, A ou F
                    'ASSURANCE' => $insurances[$s->getInsurance()], // Assurance   RC, B, B+ ou B++
                    'OPTION SKI' => $s->getInsuranceSki() ? 'OUI' : 'NON', // Option ski  OUI ou NON
                    'OPTION SLACKLINE' => $s->getInsuranceSlackline() ? 'OUI' : 'NON', // Option Slackline    OUI ou NON
                    'OPTION TRAIL' => 'NON', // Option Trail    OUI ou NON
                    'OPTION VTT' => 'NON', // Option VTT  OUI ou NON
                    'ASSURANCE COMPLEMENTAIRE' => $insurancesIj[$s->getInsuranceIj()], // Option assurance    IJ1, IJ2, IJ3 ou NON
                    'CM TYPE' => $s->getCertificateCompetition() ? 'C' : 'L', // Certificat médical - Type   L ou C
                    'CM DATE' => $s->getCertificateDate()->format('d/m/Y'), // Certificat médical - Date   format : jj/mm/aaaa
                    'CM MEDECIN' => $e(substr($s->getCertificateDoctor(), 0, 100)), // Certificat médical - Médecin    100 caractères maximum
                    'DIFFUSION COORDONNEES' => 'NON', // Diffusion des coordonnées   OUI ou NON
                    'ABONNEMENT DIRECT INFO' => 'NON', // Abonnement Direct Info  OUI ou NON
                    'ABO MAGAZINE EN LIGNE' => 'OUI', // Abonnement magazine en ligne gratuit    OUI ou NON
                ));
            }
            $writer->close();
        };

        return new StreamedResponse($callback, 200, array(
            'Content-Type'        => 'text/csv',
            'Content-Disposition' => sprintf('attachment; filename=%s', 'suresnes-escalade-'.date('Y-m-d').'.csv')
        ));
    }

    public function importFFMEAction(Request $request)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Unable to find the season with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $form = $this->createFormBuilder()
            ->add('file', 'file')
            ->add('import', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $f = fopen($form['file']->getData()->getPathname(), 'r');
            $head = fgetcsv($f, 0, "\t", '"');
            if (!is_array($head)) {
                $this->addFlash('sonata_flash_error', 'Format de fichier incorrect.');
                $this->render('AppBundle:SeasonAdmin:importFFME.html.twig', [
                    'form' => $form->createView(),
                    'action' => 'importFFME',
                    'object' => $object,
                ]);
            }
            $head = array_flip($head);  // Flip key<>values for easy access
            /*
              0 : N.LICENCE
              1 : TYPE LICENCE
              2 : ASSURANCE
              3 : ASSURANCE OPTION SKI
              4 : ASSURANCE OPTION SLACKLINE ET HIGHLINE
              5 : ASSURANCE OPTION TRAIL
              6 : ASSURANCE OPTION VTT
              7 : ASSURANCE COMPLEMENTAIRE
              8 : NOM
              9 : PRENOM
              10: SEXE
              11: ANNEE NAIS.
              12: ADRESSE1
              13: ADRESSE2
              14: C.POSTE
              15: VILLE
              16: TEL.
              17: MOBILE
              18: FAX
              19: EMAIL
              20: REVUE
              21: STRUCTURE
              22: CERTIFICAT.MEDICAL
              23: MEDECIN
              24: DATE CERTIFICAT
              25: DATE.MODIFICATION
              26: ACCIDENT_CONTACT
              27: ACCIDENT_ADRESSE1
              28: ACCIDENT_ADRESSE2
              29: ACCIDENT_CP
              30: ACCIDENT_VILLE
              31: ACCIDENT_TEL
              32: ACTIVITE
              33: DIPLOME
              34: CODE.UTILISATEUR
              35: ENVOI MAIL
              36: ENVOI SMS
              37: DATE CREATION LICENCE
             */
            if (!isset($head['N.LICENCE']) || !isset($head['NOM']) || !isset($head['PRENOM']) || !isset($head['ANNEE NAIS.'])) {
                $this->addFlash('sonata_flash_error', 'Format de fichier incorrect.');
                $this->render('AppBundle:SeasonAdmin:importFFME.html.twig', [
                    'form' => $form->createView(),
                    'action' => 'importFFME',
                    'object' => $object,
                ]);
            }

            // Prepare query
            $em = $this->getDoctrine()->getManager();

            $query = $em->getRepository('AppBundle:Subscription')
                ->createQueryBuilder('s')
                ->andWhere('s.season = :season')
                ->andWhere('s.firstName LIKE :firstName')
                ->andWhere('s.lastName LIKE :lastName')
                ->andWhere('s.birthday = :birthday')
                ->andWhere('s.status = :status')
                ->setMaxResults(1)
                ->setParameter('season', $object)
                ->setParameter('status', Subscription::STATUS_ACTIVE);

            try {
                $num = 0;
                while (($values = fgetcsv($f, 0, "\t", '"')) && !feof($f)) {
                    $num++;
                    $birthday = \DateTime::createFromFormat('d/m/Y', $values[$head['ANNEE NAIS.']]);
                    $subscription = $query
                        ->setParameter('firstName', '%'.strtr(twig_convert_encoding($values[$head['PRENOM']], 'UTF-8', 'CP1252'), ' ', '_').'%')
                        ->setParameter('lastName', '%'.strtr(twig_convert_encoding($values[$head['NOM']], 'UTF-8', 'CP1252'), ' ', '_').'%')
                        ->setParameter('birthday', $birthday->format('Y-m-d'))
                        ->getQuery()
                        ->getOneOrNullResult();

                    if (!$subscription) {
                        throw new \RuntimeException('Impossible de trouver d\'adhérent');
                    }

                    if ($subscription->getLicence() && $subscription->getLicence() != $values[$head['N.LICENCE']]) {
                        throw new \RuntimeException('Le numéro de licence ne peut pas être modifié');
                    }

                    $subscription->setLicence($values[$head['N.LICENCE']]);
                    $subscription->setLicenceDate(\DateTime::createFromFormat('d/m/Y', $values[$head['DATE CREATION LICENCE']]));
                    $em->persist($subscription);
                }

                $this->addFlash('sonata_flash_success', 'Fichier importé avec succès. '.$num.' adhérents ont leur licence FFME à jour.');

                $em->flush();
            } catch (\RuntimeException $e) {
                $this->get('logger')->error('{exception}', ['exception' => $e, 'values' => $values]);
                $this->addFlash('sonata_flash_error', 'Ligne '.$num.' : '.$e->getMessage().' '.$values[$head['PRENOM']].' '.$values[$head['NOM']].' '.$birthday->format('Y-m-d').' ('.$values[$head['N.LICENCE']].')');
            }
        }

        return $this->render('AppBundle:SeasonAdmin:importFFME.html.twig', [
            'form' => $form->createView(),
            'action' => 'importFFME',
            'object' => $object,
        ]);
    }

    public function cloneAction($id)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('NEW', $object)) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine')->getEntityManager();

        // Clone the entity
        $em->detach($object);
        $em->persist($object);
        foreach ($object->getSlots() as $slot) {
            $em->detach($slot);
            $em->persist($slot);
        }

        // Add 1 year
        $oneYear = new \DateInterval('P1Y');
        $object->getBeginAt()->add($oneYear);
        $object->getEndAt()->add($oneYear);
        $object->setTitle(sprintf(
            'Saison %d-%d',
            $object->getBeginAt()->format('Y'),
            $object->getEndAt()->format('Y')
        ));

        // Reset slots
        foreach ($object->getSlots() as $slot) {
            $slot->setIsFull(false);
        }

        $this->admin->create($object);

        $this->addFlash('sonata_flash_success', $this->admin->trans('flash_create_success', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));

        return $this->redirectTo($object);
    }
}
