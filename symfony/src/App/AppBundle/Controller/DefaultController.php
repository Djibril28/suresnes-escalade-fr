<?php

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        var_dump($_SESSION);
        return array('name' => $name);
    }

    /**
     * @Route("/sync-mailchimp")
     */
    public function mailchimpAction()
    {
        $season = $this->getDoctrine()->getRepository('AppBundle:Season')->findCurrent();

        $response = $this->get('mailchimp')->syncList($season);

        $data = array(
            'add_count' => $response['add_count'],
            'update_count' => $response['update_count'],
            'error_count' => $response['error_count'],
        );

        return JsonResponse::create($data);
    }
}
