<?php

namespace App\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\AppBundle\Entity\Subscription;
use App\AppBundle\Event\SubscriptionEvent;
use App\AppBundle\Form;
use App\AppBundle\AppEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Subscription controller.
 *
 * @Route("/simple")
 */
class SimpleSubscriptionController extends Controller
{
    const CURRENT_SEASON = true;
    const ANY_SEASON = false;

    /**
     * Lists all Subscription entities.
     *
     * @Route("/", name="subscription")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getCurrentUser();
        $entities = $em->getRepository('AppBundle:Subscription')->findByUser($user);

        $currentSeason = $em->getRepository('AppBundle:Season')->findCurrent();
        $currentSubscription = $em->getRepository('AppBundle:Subscription')->findOneBy(array(
            'userId' => $user->getId(),
            'season' => $currentSeason,
        ));

        return array(
            'entities' => $entities,
            'season' => $currentSeason,
            'subscription' => $currentSubscription,
        );
    }

    /**
     * @Route("/inscription", name="simple_subscription")
     * @Route("/inscription.php", name="simple_subscription_legacy")
     * @Template()
     */
    public function formAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->query->has('new')) {
            $request->getSession()->remove('SUBSCRIPTION');

            return $this->redirect($this->generateUrl('simple_subscription'));
        }

        $subscription = $this->findPrivateSubscription($request, static::CURRENT_SEASON);

        if ($subscription->isActive()) {
            return $this->redirect($this->generateUrl('simple_subscription_accepted'));
        }

        $form = $this->createForm(new Form\SimpleSubscriptionType($subscription->getSeason()), $subscription, array(
            'action' => $this->generateUrl('simple_subscription'),
        ));
        $form->add('submit', 'submit', array('label' => 'Valider'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($subscription);
            $em->flush();

            $request->getSession()->set('SUBSCRIPTION', $subscription->getId().'/'.$subscription->getKey());

            $event = new SubscriptionEvent($subscription);
            $this->get('event_dispatcher')->dispatch(AppEvents::SUBSCRIPTION_SUBMITTED, $event);

            return $this->redirect($this->generateUrl('simple_subscription_submitted'));
        }

        return array(
            'subscription' => $subscription,
            'form'         => $form->createView(),
        );
    }

    /**
     * @Route("/confirmation", name="simple_subscription_submitted")
     * @Template()
     */
    public function submittedAction(Request $request)
    {
        $subscription = $this->findPrivateSubscription($request, static::CURRENT_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        return array(
            'subscription' => $subscription,
        );
    }

    /**
     * @Route("/inscription-acceptee", name="simple_subscription_accepted")
     * @Template()
     */
    public function acceptedAction(Request $request)
    {
        $subscription = $this->findPrivateSubscription($request, static::ANY_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        if (!$subscription->isActive()) {
            return $this->redirect($this->generateUrl('simple_subscription'));
        }

        return array(
            'subscription' => $subscription,
        );
    }

    /**
     * @Route("/inscription/imprimer", name="simple_subscription_print")
     * @Template()
     */
    public function printAction(Request $request)
    {
        $subscription = $this->findPrivateSubscription($request, static::CURRENT_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        return array(
            'subscription' => $subscription,
        );
    }

    /**
     * @Route("/inscription-justificatif", name="simple_subscription_voucher")
     * @Template()
     */
    public function voucherAction(Request $request)
    {
        $subscription = $this->findPrivateSubscription($request, static::ANY_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        return array(
            'subscription' => $subscription,
        );
    }

    /**
     * @Route("/certificat/{id}", name="simple_subscription_certificat")
     * @ParamConverter("subscription", class="AppBundle:Subscription")
     */
    public function certificateAction(Request $request, Subscription $subscription)
    {
        if ($subscription->getCertificateFile()) {
            return BinaryFileResponse::create($subscription->getCertificateFilePath());
        }

        throw $this->createNotFoundException();
    }

    private function findPrivateSubscription(Request $request, $current = true)
    {
        $session = $request->getSession();
        $ref = $key = null;

        if ($request->query->has('ref')) {
            $ref = $request->query->get('ref');
            $key = $request->query->get('key');
        } elseif ($session->has('SUBSCRIPTION')) {
            list($ref, $key) = explode('/', $session->get('SUBSCRIPTION'), 2);
        }

        $em = $this->getDoctrine()->getManager();
        $season = $em->getRepository('AppBundle:Season')->findCurrent();
        $subscription = false;

        if ($key && $ref) {
            $criteria = array(
                'id' => $ref,
                'key' => $key,
            );

            if (static::CURRENT_SEASON === $current) {
                $criteria['season'] = $season;
            }

            $subscription = $em->getRepository('AppBundle:Subscription')->findOneBy($criteria);
        }

        if ($subscription) {
            $session->set('SUBSCRIPTION', $ref.'/'.$key);
        } else {
            $session->remove('SUBSCRIPTION');

            $subscription = new Subscription();
            $subscription->setSeason($season);
            $subscription->setIsRenewal(false);
        }

        return $subscription;
    }
}
