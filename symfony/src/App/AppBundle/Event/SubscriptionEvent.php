<?php

namespace App\AppBundle\Event;

use App\AppBundle\Entity\Subscription;
use Symfony\Component\EventDispatcher\Event;

class SubscriptionEvent extends Event
{
	private $subscription;

	public function __construct(Subscription $subscription)
	{
		$this->subscription = $subscription;
	}

	public function getSubscription()
	{
		return $this->subscription;
	}
}
