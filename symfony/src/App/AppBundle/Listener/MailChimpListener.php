<?php

namespace App\AppBundle\Listener;

use App\AppBundle\Event\SubscriptionEvent;
use App\AppBundle\Entity\Subscription;
use App\AppBundle\Service\MailChimp;

class MailChimpListener
{
    private $mailchimp;

    public function __construct(MailChimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function onSubscriptionUpdated(SubscriptionEvent $event)
    {
        $this->mailchimp->syncSubscription($event->getSubscription());
    }
}